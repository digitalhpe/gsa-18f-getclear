/**
 * Created by jimpeters on 6/23/15.
 */
var express = require('express');
var router = express.Router();
var Q = require('q');

var processProducts;
var dataLayer;
var utils;

router.get('/', function(req, res) {
    dataLayer = req.app.locals.dataLayer;
    utils = req.app.locals.utils;

    if (req.query.UPC === undefined) {
        res.status(400).json({"result": 'get parameter missing'});
        return;
    }

    var thisProduct = req.query.UPC;

    if (!thisProduct) {
        res.status(400).json({"result": 'get parameter empty'});
        return;
    }

    var thisResult = {};
    var products = [];

    dataLayer.findProductUPC(thisProduct, function(err, data) {
        if (err) {
            utils.handleError(err, res);
        } else {
            if  (data === null || data.length === 0) {
                utils.handleError(new Error('Not Found'), res);
            } else {
                var thisData = JSON.parse(data);
                if (thisData.error && thisData.error.code === "SERVER_ERROR") {
                    utils.handleError(new Error('Server error'), res);
                } else if (thisData.error && thisData.error.code === "NOT_FOUND") {
                    utils.handleError(new Error('Not Found'), res);
                } else {
                    var upcItemsArray = [];

                    if (thisData && thisData.results && thisData.results.length > 0) {
                        for (var i = 0; i < thisData.results.length; i++) {
                            if (thisData.results[i].openfda.upc && thisData.results[i].openfda.upc[0] !== "") {
                                var upcItem = {
                                    upc: thisData.results[i].openfda.upc[0],
                                    manufacturer_name: thisData.results[i].openfda.manufacturer_name[0],
                                    brand_name: thisData.results[i].openfda.brand_name[0],
                                    package_label_principal_display_panel: thisData.results[i].package_label_principal_display_panel[0],
                                    active_ingredient: "",
                                    inactive_ingredient: "",
                                    image_urls:[],
                                    ingredients:[]
                                };

                                if (thisData.results[i].active_ingredient) {
                                    upcItem.active_ingredient = thisData.results[i].active_ingredient[0];
                                }

                                if (thisData.results[i].inactive_ingredient) {
                                    upcItem.inactive_ingredient = thisData.results[i].inactive_ingredient[0];
                                }

                                upcItemsArray.push(upcItem);
                            }
                        }
                    }

                    processProducts = upcItemsArray;

                    processItems(processProducts.shift(), thisResult, products, req, res);
                }
            }
        }
    });
});

function processItems(thisItem, thisResult, products, req, res) {
    if (thisItem) {
        Q.denodeify(dataLayer.findFactualUPC(thisItem.upc).then(function(upcdata) {
            if (upcdata.data.length > 0) {
                if (upcdata.data[0].image_urls) {
                    thisItem.image_urls = upcdata.data[0].image_urls;
                }
                if (upcdata.data[0].ingredients) {
                    thisItem.ingredients = upcdata.data[0].ingredients;
                }
            }

            products.push(thisItem);

            return processItems(processProducts.shift(), thisResult, products, req, res);
        }, function(err){
            console.log('Get Factual error received:', err);
        }));
    }
    else {
        thisResult.products = products;
        res.status(200).json(thisResult);
    }
}

module.exports = router;
