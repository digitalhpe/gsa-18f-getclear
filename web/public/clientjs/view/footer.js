GC.View.footer = Backbone.Marionette.ItemView.extend({
  template  : GC.Template.footer,

  render: function(){
    this.$el.html(this.template(GC.Data));

    return this;
  }
});
