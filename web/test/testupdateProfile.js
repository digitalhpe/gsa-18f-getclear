/**
 * Created by jimpeters on 6/28/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testupdateProfile', function () {
    it ('should return 400 for missing parameters', function (done) {
        var input = {};

        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/updateProfile')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('parameter missing');

                done();
            });
    });

    it ('should return 400 for missing email_addr', function (done) {
        var input =
        {
            "parameters": {
                "name": "Joe Peters",
                "ingredients": [],
                "products": []
            }
        };

        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/updateProfile')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('email parameter missing');

                done();
            });
    });

    it ('should return 200 for a valid call', function (done) {
        var input =
        {
            "parameters": {
                "name": "Joe Peters",
                "email_addr" : "joe.peters@hp.com",
                "ingredients": [],
                "products": []
            }
        };

        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/updateProfile')
            .send(input)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});
