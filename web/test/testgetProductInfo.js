/**
 * Created by jimpeters on 6/28/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testgetProductInfo', function () {
    it ('should return 400 for missing parameter', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductInfo')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter missing');

                done();
            });
    });

    it ('should return 400 for empty parameter', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductInfo?ProductName=')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter empty');

                done();
            });
    });

    it ('should return 404 for not found', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductInfo?ProductName=somethingbad')
            .expect('Content-Type', /json/)
            .expect(404)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Not Found');

                done();
            });
    });

    it ('should return 500 for SERVER_ERROR', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductInfo?ProductName=somethingreallybad')
            .expect('Content-Type', /json/)
            .expect(500)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Server error');

                done();
            });
    });

    it ('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductInfo?ProductName=Carmex')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                done();
            });
    });
});

