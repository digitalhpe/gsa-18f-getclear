GC.View.search_results = Backbone.Marionette.CompositeView.extend({

    template: GC.Template.search_results,
    childView: GC.View.search_result_item,

    collectionEvents: {
        "add": "render"
    },

    events: {
        "click #newSearch": "newSearch"
    },

    CheckBoxState: Backbone.Model.extend({}),

    CheckBoxStateCollection: Backbone.Collection.extend({
        model: this.CheckBoxState
    }),

    searchTermComplete: false,
    irritantSearchComplete: false,

    initialize: function () {
        var _self = this;

        _self.checkboxCollection = new _self.CheckBoxStateCollection();
        this.listenTo(_self.checkboxCollection, 'add remove reset', this.updateDoNotBuyView);

        var searchTerm = GC.Data.app.searchTerm;
        var irritants = GC.Data.app.selectedIrritants;

        if (searchTerm) {
            GC.WebServiceCalls.getProductInfo(searchTerm, function (data) {

                var resultsCollection = _self.parseResults(data.products, _self);

                if (!_self.collection) {
                    _self.collection = resultsCollection;
                } else {
                    _self.collection.add(resultsCollection.models);
                }

                _self.render();
                if (!resultsCollection) {
                    searchTermComplete = true;
                    _self.updateSearchResultsHeader();
                }

            }, function () {
                searchTermComplete = true;
                $('#data-load-failure').show();
                _self.updateSearchResultsHeader();
            });
        } else {
            searchTermComplete = true;
        }

        if (irritants.length > 0) {
            var params = {"ingredients": irritants};
            GC.WebServiceCalls.getIngredientInfo(params, function (data) {

                var resultsCollection = _self.parseResults(data.products, _self);

                if (!_self.collection) {
                    _self.collection = resultsCollection;
                } else {
                    _self.collection.add(resultsCollection.models);
                }
                _self.render();

                if (!resultsCollection) {
                    irritantSearchComplete = true;
                    _self.updateSearchResultsHeader();
                }

            }, function () {
                irritantSearchComplete = true;
                $('#data-load-failure').show();
                _self.updateSearchResultsHeader();
            });
        } else {
            irritantSearchComplete = true;
        }
    },

    /**
     * Renders the child views
     *
     * @param collectionView
     * @param childView
     */
    attachHtml: function (collectionView, childView) {
        collectionView.$("#results-container").append(childView.el);
        var titleDiv = collectionView.$("#title");

        if (titleDiv.text().trim() === "Searching...") {

            var count = this.collection.length;
            var searchTerm = GC.Data.app.searchTerm;
            var irritants = GC.Data.app.selectedIrritants;
            var text = "";
            var irritantsString = "";

            for (var i = 0; i < irritants.length; i++) {
                irritantsString += irritants[i];

                if (i < irritants.length - 1) {
                    irritantsString += ", ";
                }
            }

            if (searchTerm && irritants.length > 0) {
                text += ("'" + searchTerm + "' and " + irritantsString);
            }
            else if (searchTerm) {
                text += "'" + searchTerm + "'";
            } else {
                text += irritantsString;
            }

            if (count > 1) {
                titleDiv.html("<h3>" + count + " Search Results for <b>" + text + "</b></h3>");
            } else {
                titleDiv.html("<h3>" + count + " Search Result for <b>" + text + "</b></h3>");
            }
        }
    },

    parseResults: function (results) {
        var User = Backbone.Model.extend({});
        var UserCollection = Backbone.Collection.extend({
            model: User
        });

        if (!jQuery.isEmptyObject(results)) {
            return new UserCollection(results);
        } else {
            return null;
        }
    },

    /**
     * Called before the child views are rendered
     */
    onRenderTemplate: function () {
        var _self = this;
        _self.updateProfileLinkVisibility();
    },

    /**
     * Called after everything has been rendered
     */
    onRender: function () {

        var _self = this;

        $(".do-not-buy-checkbox").change(function () {
            $('#submit-success').hide();
            $('#submit-failure').hide();
            var item = _self.collection.where(
                {
                    brand_name: this.id,
                    upc: this.classList[1]
                }
            )[0];
            if (this.checked) {
                _self.checkboxCollection.add(item);
            } else {
                _self.checkboxCollection.remove(item);
            }
        });

        $("#submit-do-not-buy-list").click(function () {

            if (GC.Data.app.user.authenticated === true) {
                _self.checkboxCollection.each(function (item) {
                    var data = {
                        upc: item.attributes.upc,
                        brand_name: item.attributes.brand_name,
                        image_url: item.attributes.image_urls[0]
                    };

                    if (isDoNotBuyDuplicate(data, GC.Data.app.user.doNotBuyList)) {
                        // This product is already in the list
                    } else {
                        GC.Data.app.user.doNotBuyList.push(data);
                    }
                });

                GC.WebServiceCalls.updateProfile(function () {
                    _self.checkboxCollection.reset();
                    $(".do-not-buy-checkbox").attr("checked", false);
                    $('#submit-success').show();
                }, function () {
                    $('#submit-failure').show();
                });
            } else {
                //Not Authenticated, show modal
                $('#search-login-modal').modal();
            }

        });

        $('.product_image').each(function (i, obj) {
            if (obj.src.endsWith('/')) {
                obj.src = '/img/no_image_lg.png';
            }
        });

        $('#newSearch').show();

        GC.Utils.setGoogleAuthListener(function () {
                $('#search-login-modal').modal('hide');
                _self.updateProfileLinkVisibility();
            }
        );
    },

    onShow: function () {
        $('#searchMessage').hide();
    },

    updateProfileLinkVisibility: function () {
        if (GC.Data.app.user.authenticated) {
            $('#view-my-profile-link').show();
        } else {
            $('#view-my-profile-link').hide();
        }
    },

    updateDoNotBuyView: function () {

        var doNotBuyCount = this.checkboxCollection.length;
        if (doNotBuyCount <= 0) {
            $("#submit-do-not-buy-list").attr('disabled', 'disabled');
        } else {
            $("#submit-do-not-buy-list").removeAttr('disabled');
        }

        var listDiv = $("#do-not-buy-list");
        listDiv.html("");
        this.checkboxCollection.each(function (item) {
            var name = item.attributes.brand_name;
            listDiv.append("<div class=\"col-md-12 search-result-no-buy\"><input type=\"checkbox\" id=\"" + name + "\" disabled=\"disabled\" checked=\"checked\"/><label>" + name + "</label></div>");
        });
    },

    newSearch: function () {
        searchTermComplete = false;
        irritantSearchComplete = false;
        GC.Router.navigate("#home", {trigger: true, replace: false});
    },

    updateSearchResultsHeader: function () {
        if (irritantSearchComplete && searchTermComplete) {
            $("#title").html("<h3>No Search Results</h3>");
            $('#newSearch').show();
        }
    }
});

var isDoNotBuyDuplicate = function (item, list) {
    for (var i = 0, len = list.length; i < len; i++) {
        if ((list[i].upc === item.upc) && (list[i].brand_name === item.brand_name) && (list[i].image_url === item.image_url))
            return true;
    }
    return false;
};

