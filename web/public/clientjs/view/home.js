var showLoginContainer = false;

GC.View.home = Backbone.Marionette.ItemView.extend({
    template: GC.Template.home,
    events: {
        "click #searchButton": "performSearch",
        "keyup #searchTextBox" : "keyPressEventHandler"
    },
    performSearch: function () {

        var searchText = $("#searchTextBox").val();
        // TODO: Add irritant check here
        if (!searchText && GC.Data.app.selectedIrritants.length === 0) {
            $('#searchMessage').show();
        } else {
            GC.Data.app.searchTerm = searchText;
            GC.Router.navigate("#search_results", {trigger: true, replace: false});
        }
    },
    keyPressEventHandler : function(event){
        if(event.keyCode === 13){
            this.$("#searchButton").click();
        }
    },
    render: function () {
        var _self = this;
        GC.Utils.setGoogleAuthListener(function () {
                showLoginContainer = true;
                setProfileArea();
            }
        );

        if (!GC.Data.app.irritants) {
            GC.WebServiceCalls.getIrritants(function (data, status) {
                GC.Data.app.irritants = data.irritants;
                _self.render();
                $('#searchMessage').hide();
                setupIrritantCategoryTabs();
                addIrritantCheckHandler();
            }, function (data) {
                console.log(data.responseText);
            });
        }

        this.$el.html(this.template(GC.Data));
        return this;
    },
    onShow: function () {
        setupIrritantCategoryTabs();
        addIrritantCheckHandler();

        GC.Data.app.selectedIrritants = [];
        $('#searchMessage').hide();

        setProfileArea();
    }
});

function handleAuthClick(event) {
    auth2.signIn();
}

function setProfileArea() {
    if (GC.Data.app.user.authenticated === true) {
        $('#username').text("Welcome, " + GC.Data.app.user.name);
        $("#userImage").attr("src", GC.Data.app.user.imageUrl);
        $("#userIrritantSize").text("You have " + GC.Data.app.user.knownIrritantsList.length + " irritants listed in your profile.");
        $('#authenticatedContainer').show();
        $('#nonAuthenticatedContainer').hide();
    } else {
        $('#nonAuthenticatedContainer').show();
        $('#authenticatedContainer').hide();
    }

    if (showLoginContainer) {
        $('#loginContainer').show();
    }
}

function setupIrritantCategoryTabs() {
    $("#irritantTab a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
}

function addIrritantCheckHandler() {
    $(".irritant-check").change(function () {
        var irritantName = this.id;

        if (this.checked) {
            GC.Data.app.selectedIrritants.push(irritantName);
        } else {
            var index = GC.Data.app.selectedIrritants.indexOf(irritantName);

            if (index > -1) {
                GC.Data.app.selectedIrritants.splice(index, 1);
            }
        }
    });
}

function logout() {
    GC.Utils.googleSignOut();
}

