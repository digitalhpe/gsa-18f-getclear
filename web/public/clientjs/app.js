/**
 * Created by equisenb on 6/23/15.
 */

"use strict";

var GC = {
    "App": {},
    "Data": {},
    "View": {},
    "Model": {},
    "Router": {},
    "Template": {},
    "Collection": {},
    "WebServiceCalls": {},
    "Utils": {}
};

GC.App = Marionette.Application.extend({
    initialize: function (options) {
        GC.Data = GC_Data;
        GC.Data.View = {};
        GC.WebServiceCalls = web_service_calls;
        GC.Utils = utils;
    }
});

var sync = Backbone.sync;
Backbone.sync = function (method, model, options) {
    sync(method, model, options);
};
