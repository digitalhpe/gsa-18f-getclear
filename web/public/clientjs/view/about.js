GC.View.about = Backbone.Marionette.ItemView.extend({

    template: GC.Template.about,

    initialize: function () {

    },

    render: function () {
        this.$el.html(this.template());
    }
});