/**
 * Created by jimpeters on 6/28/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testgetProductUPC', function () {
    it ('should return 400 for missing UPC', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductUPC')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter missing');

                done();
            });
    });

    it ('should return 400 for empty UPC', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductUPC?UPC=')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter empty');

                done();
            });
    });

    it ('should return 404 for not found', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductUPC?UPC=999999999999')
            .expect('Content-Type', /json/)
            .expect(404)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Not Found');

                done();
            });
    });

    it ('should return 500 for SERVER_ERROR', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductUPC?UPC=somethingreallybad')
            .expect('Content-Type', /json/)
            .expect(500)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Server error');

                done();
            });
    });

    it ('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProductUPC?UPC=0083078011307')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                done();
            });
    });
});