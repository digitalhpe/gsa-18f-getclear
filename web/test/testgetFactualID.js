/**
 * Created by jimpeters on 7/1/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testgetFactualID', function () {
    it ('should return 400 for missing FactualID', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getFactualID')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter missing');

                done();
            });
    });

    it ('should return 400 for empty FactualID', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getFactualID?FactualID=')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter empty');

                done();
            });
    });

    it ('should return 404 for not found', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getFactualID?FactualID=222-222-2222-2222')
            .expect('Content-Type', /json/)
            .expect(404)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Not Found');

                done();
            });
    });

    it ('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getFactualID?FactualID=4087dee1-228e-48d2-a9a3-3665efb00361')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                done();
            });
    });
});