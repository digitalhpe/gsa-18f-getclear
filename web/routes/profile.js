/**
 * Created by jimpeters on 6/25/15.
 */
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var dataLayer = req.app.locals.dataLayer;
    var utils = req.app.locals.utils;

    if (req.query.email_addr === undefined) {
        res.status(400).json({"result": 'get parameter missing'});
        return;
    }

    var email_addr = req.query.email_addr;

    if (!email_addr) {
        res.status(400).json({"result": 'get parameter empty'});
        return;
    }

    dataLayer.getProfile(email_addr, function(err, data) {
        if (err) {
            utils.handleError(err, res);
        } else if (data === null || data.length === 0) {
            utils.handleError(new Error('Not Found'), res);
        } else {
            res.status(200).json(data);
        }
    });
});

router.post('/', function(req, res) {
    var dataLayer = req.app.locals.dataLayer;
    var utils = req.app.locals.utils;

    var parameters = req.body.parameters;
    if (parameters === undefined) {
        res.status(400).json({"result": 'parameter missing'});
        return;
    }

    var emailAddr = parameters.email_addr;
    if (emailAddr === undefined) {
        res.status(400).json({"result": 'email parameter missing'});
        return;
    }

    dataLayer.insertProfile(emailAddr, parameters, function(err, data) {
        if (err) {
            utils.handleError(err, res);
        } else {
            res.status(200).json({"result": "OK"});
        }
    });
});

module.exports = router;

