GC.View.header = Backbone.Marionette.ItemView.extend({
  template  : GC.Template.header,
  events: {
      "click #logout": "logout"
  },
  logout: function () {
      GC.UserLogout();
      GC.Layout.header.show(new GC.View.header());
  },
  render: function(){
    this.$el.html(this.template(GC.Data));
    return this;
  }
});
