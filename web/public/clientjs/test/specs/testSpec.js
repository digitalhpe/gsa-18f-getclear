'use strict';

describe("jasmine_unit_test_results", function () {

    beforeEach(function () {
        var App = new GC.App({container: "div.container-fluid"});
        GC.Layout = new GC.View.layout();
    });

    it("can determine a duplicate product object", function () {

        var obj1 = {upc: 1, brand_name: 'brand', image_url: 'url'};
        var obj2 = {upc: 2, brand_name: 'brand', image_url: 'url'};
        var obj3 = {upc: 3, brand_name: 'brand', image_url: 'url'};
        var obj4 = {upc: 4, brand_name: 'brand', image_url: 'url'};
        var obj5 = {upc: 5, brand_name: 'brand', image_url: 'url'};
        var obj6 = {upc: 6, brand_name: 'brand', image_url: 'url'};
        var obj7 = {upc: 7, brand_name: 'brand', image_url: 'url'};
        var obj8 = {upc: 8, brand_name: 'brand', image_url: 'url'};
        var obj9 = {upc: 9, brand_name: 'brand', image_url: 'url'};
        var obj10 = {upc: 10, brand_name: 'brand', image_url: 'url'};

        var list = [obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10];

        var testObj = obj1;
        expect(isDoNotBuyDuplicate(testObj, list)).toBe(true);

        testObj = obj2;
        expect(isDoNotBuyDuplicate(testObj, list)).toBe(true);

        testObj = {upc: 11, brand_name: 'brand', image_url: 'url'};
        expect(isDoNotBuyDuplicate(testObj, list)).toBe(false);
    });

    it("builds a backbone collection from a list", function () {

        var results = new GC.View.search_results();

        expect(results.parseResults({})).toBe(null);

        expect(results.parseResults({
            test: 'test',
            test2: 'test2',
            test3: 'test3',
            test4: 'test3'
        }).size()).toBe(1);

        expect(results.parseResults([
            'test',
            'test2',
            'test3',
            'test3'
        ]).size()).toBe(4);

        expect(results.parseResults([
            'test'
        ]) instanceof Backbone.Collection).toBe(true);
    });

    it("verifies object construction", function () {
        var product_details = new GC.View.product_details({param: "123456789"});
        expect(product_details.upc).toBe("123456789");

        product_details = new GC.View.product_details({upc: "123456789"});
        expect(product_details.upc).toBe(undefined);

        product_details = new GC.View.product_details({param: ""});
        expect(product_details.upc).toBe("");

        product_details = new GC.View.product_details({param: "fake_upc"});
        expect(product_details.upc).toBe("fake_upc");

    });

    it("finds the intersection of know irritants and product ingredients", function () {

        var product_details = new GC.View.product_details();

        GC.Data.app.irritants = {
            irritantList1: [1, 2, 3, 4, 5],
            irritantsList2: [6, 7, 8, 9, 10],
            irritantsList3: [11, 12, 13, 14, 15]
        };

        product_details.product = {};
        product_details.product.ingredients = ['ingredient1', 'ingredient2', 'ingredient3', 'ingredient4'];
        product_details.product.active_ingredient = "ingredient5, ingredient6, ingredient7, ingredient8";
        product_details.product.inactive_ingredient = "ingredient9, ingredient10, ingredient11, ingredient12";
        product_details.determineIrritants();
        expect(product_details.product.knownIrritants.length).toBe(12);

        GC.Data.app.irritants = {};
        product_details.determineIrritants();
        expect(product_details.product.knownIrritants.length).toBe(0);

        GC.Data.app.irritants = {
            irritantList1: ['ingredient6'],
            irritantsList2: ['ingredient10'],
            irritantsList3: ['fake']
        };

        product_details.determineIrritants();
        expect(product_details.product.knownIrritants.length).toBe(2);

    });

    it("prepares the known irritant list for sending", function () {
        var product_details = new GC.View.product_details();

        GC.Data.app.selectedIrritants = [1, 2, 3, 4, 5];
        product_details.saveIrritants();
        expect(GC.Data.app.user.knownIrritantsList.length).toBe(5);

        GC.Data.app.user.knownIrritantsList = [];
        ;
        GC.Data.app.selectedIrritants = [];
        product_details.saveIrritants();
        expect(GC.Data.app.user.knownIrritantsList.length).toBe(0);

        GC.Data.app.user.knownIrritantsList = [];
        GC.Data.app.selectedIrritants = [1, 2, 3, 4, 4];
        product_details.saveIrritants();
        expect(GC.Data.app.user.knownIrritantsList.length).toBe(4);
    });
});