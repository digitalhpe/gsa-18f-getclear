var web_service_calls = web_service_calls || {};

web_service_calls.baseUrl = "api/";

web_service_calls.getProductInfo = function (productName, success, error) {
    $.ajax({
        type: "GET",
        url: GC.WebServiceCalls.baseUrl + "getProductInfo?ProductName=" + productName,
        contentType: "application/json",

        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },

        success: function (data, status) {
            success(data, status);
        },
        error: function (data) {
            error(data);
        }
    });
};

web_service_calls.getIngredientInfo = function (ingredientsList, success, error) {
    $.ajax({
        type: "POST",
        url: GC.WebServiceCalls.baseUrl + "getIngredientInfo",
        contentType: "application/json",
        data: JSON.stringify(ingredientsList),

        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },

        success: function (data, status) {
            success(data, status);
        },
        error: function (data) {
            error(data);
        }
    });
};

web_service_calls.getIrritants = function (success, error) {
    $.ajax({
        type: "GET",
        url: GC.WebServiceCalls.baseUrl + "getIrritants",
        contentType: "application/json",

        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },

        success: function (data, status) {
            success(data, status);
        },
        error: function (data) {
            error(data);
        }
    });
};

web_service_calls.getProfile = function (email, success, error) {
    $.ajax({
        type: "GET",
        url: GC.WebServiceCalls.baseUrl + "getProfile?email_addr=" + email,
        contentType: "application/json",

        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },

        success: function (data, status) {
            success(data, status);
        },
        error: function (data) {
            error(data);
        }
    });
};

web_service_calls.updateProfile = function (success, error) {
    var user = GC.Data.app.user;
    var params = {"parameters": {"name" : user.name, "email_addr" : user.email, "ingredients" : user.knownIrritantsList, "products" : user.doNotBuyList}};

    $.ajax({
        type: "POST",
        url: GC.WebServiceCalls.baseUrl + "updateProfile",
        contentType: "application/json",
        data: JSON.stringify(params),

        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },

        success: function (data, status) {
            success(data, status);
        },
        error: function (data) {
            error(data);
        }
    });
};

web_service_calls.getProductUpc = function (upc, success, error) {
    $.ajax({
        type: "GET",
        url: GC.WebServiceCalls.baseUrl + "getProductUPC?UPC=" + upc,
        contentType: "application/json",

        headers: {
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },

        success: function (data, status) {
            success(data, status);
        },
        error: function (data) {
            error(data);
        }
    });
};