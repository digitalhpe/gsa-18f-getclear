GC.Router = Backbone.Marionette.AppRouter.extend({
    routes: {
        "": "home",
        "home": "home",
        "profile": "profile",
        "search_results": "search_results",
        "product_details": "product_details",
        "product_details/:product_id": "product_details",
        "about": "about",
        "*path": "not_found"
    },

    initialize: function () {
        //This runs after the route handler has executed
        this.on("route", function (route, params) {

            this.scroll();
            $('[data-toggle="popover"]').popover();   // needed for subsequent page loads
            $(window).on('resize scroll', this.scroll);
        });
    },

    home: function () {
        GC.Layout.content.show(new GC.View.home());
    },

    profile: function () {
        GC.Layout.content.show(new GC.View.profile());
    },

    search_results: function (id) {

        var view = new GC.View.search_results({
            collection: null
        });
        GC.Layout.content.show(view);
    },

    product_details: function (product) {
        GC.Layout.content.show(new GC.View.product_details({param: product}));
    },

    about: function () {
        GC.Layout.content.show(new GC.View.about());
    },

    scroll: function () {
        var scrollable;
        scrollable = ($(window).scrollTop() + $(window).height() === $(document).height()) ? false : true;
        $('.scrollbar-gradient').toggleClass('hidden', !scrollable);
    }
})
;
