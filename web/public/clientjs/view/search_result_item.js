GC.View.search_result_item = Backbone.Marionette.ItemView.extend({

    template: GC.Template.search_result_item,
    tagName: "li",

    initialize: function () {
    }
});
