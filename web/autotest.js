// Automated UI tests (9)

var pageTimeOut = 6000; //Global page load timeout
var page = require('webpage').create();
var startTime = new Date();
var startTestTime = new Date();
var fs = require('fs');
var fpath = 'web/output/phantom_test_results.xml';
var curTest = "Start";
var fileOut = '';
var totTests=0;

var writeReport = function(failures){
    fileOut2='<testsuite name="PhantomJS Tests" tests="'+totTests+'" failures="'+failures+'" errors="0" skipped="0" timestamp="'+new Date()+'" time="'+(new Date()-startTime)/1000+'">\n'+fileOut;
    fileOut2+='</testsuite>\n';
    fs.write(fpath,fileOut2,'w');
};

var fail = function(msg){
        logTest(false);
        writeReport('1');
        page.render("errorpage.png");
        console.log(msg+" ("+(new Date()-startTime)/1000+" sec)");
        phantom.exit(1);
};

var startTest = function(val){
    curTest=val;
    console.log(val);
    startTestTime=new Date();
};

var logTest = function(passed){
    var dur = (new Date()-startTestTime)/1000;
    var pstr='1';
    totTests++;
    if(passed) pstr='0';
    fileOut+='<testcase classname="PhantomJS test" name="'+curTest+'" time="'+dur+'"/>\n';
    console.log("::"+fileOut);
};

var domLoaded = function(){
    return page.evaluate(function() {
                return document.readyState == 'complete';
    });
};
var clickHref = function(path){
    page.evaluate(function(path) {
                  document.location.href=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().href;
                  },path);
};
var checkBox = function(path){
    var el= page.evaluate(function(path) {
                      var obj=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext();
                      obj.checked=true;
    },path);
};
var clickOn = function(path){
    var el= page.evaluate(function(path) {
                var obj=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext();
                obj.click();
    },path);
    
};
var setText = function(path,value){
    var el= page.evaluate(function(path,value) {
            var obj=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext();
            obj.value = value;
    },path,value);
    
};
var elByXPath = function(path){
    var elm= page.evaluate(function(path) {
        return document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext();
    },path);
    if(elm) return elm.innerHTML; else return false;
};
var eleHTML = function(val){
    var elm = page.evaluate(function(val){
                            return document.getElementById(val);
                            },val);
    if(elm) return elm.innerHTML; else return false;
};
var eleBySelector = function(val){
    var elm = page.evaluate(function(val){
                            return document.querySelector(val);
                            },val);
    if(elm) return elm.innerHTML; else return false;
};
var eleContains = function(val,str){
    var elm = page.evaluate(function(val){
                            return document.getElementById(val);
                            },val);
    if(elm) return elm.innerHTML; else return false;

    if(elm.indexOf(str)>=0) return true;
    return false;
};

function test1() {
    startTest("Test 1 - Is 'create an account' link present?");
    page.open('https://getclear18f.com', function(status) {
        if(status !== 'success') {
              fail('Test 1 failed - Unable to access URL');
        } else {
              var ret=elByXPath('//*[@id=\"nonAuthenticatedContainer\"]/div[1]/div');
              if(ret==false) {
                    fail("Automated test 1 failed! Couldn't find 'Create an Account' on home page while signed out.");
              }
              logTest(true);
              startTest("Test 2 - Enter 'cream' into search field and verify that we get search results");

            var res=setText('//*[@id=\"searchTextBox\"]','cream'); //div/section[1]/div/div/div/div/div[1]/div[2]/div
              if(res==false) {
                    fail("Automated test 2 failed! Couldn't find home search box!");
              }
              clickOn('//*[@id=\"searchButton\"]'); //search!
//              console.log("isloaded="+domLoaded());
              setTimeout(test2,pageTimeOut);
        }
    });
}

function test2() {
    var ret=elByXPath('//*[@id=\"title\"]/h3');
    if(ret==false || ret.indexOf("Search Result")==-1) {
        fail("Automated test 2 failed! Couldn't get search results for 'cream'");
    }
    page.open('https://getclear18f.com', function(status) {
        if(status !== 'success') {
              fail('Test 2 failed - Unable to access URL');
        } else {
              setTimeout(test3,pageTimeOut);
        }
    });
}

function test3() {
    logTest(true);
    startTest("Test 3 - check Cinnamic alcohol box, add to irritants, and see do-not-buy");
    //checkBox('//*[@id=\"Aluminum\"]'); //check ALUMINUM
    clickOn('//*[@id=\"Cinnamic alcohol\"]'); //check Cinnamic alcohol
    clickOn('//*[@id=\"searchButton\"]'); //search!
    setTimeout(test3b,pageTimeOut);
}

function test3b() {
//    console.log("test3 section two");
    var ret=elByXPath('//*[@id=\"title\"]/h3');
    if(ret==false || ret.indexOf("Search Result")==-1) {
        fail("Automated test 3 failed! Couldn't get search results for the irritant Cinnamic alcohol");
    }
    clickOn("//*[contains(@class,'do-not-buy-checkbox')]")
    setTimeout(test4,100);
}

function test4() {
    logTest(true);
    startTest("Test 4 - check an item for no-buy in search results and see it appear in list at bottom of page");
    var res=elByXPath("//*[contains(@class,'search-result-no-buy')]");
    if(res==false) {
        fail("Automated test 4 failed!!  Checked a no-buy item, and it didn't show up in list at bottom!");
    }

    //log into gmail
    //https://accounts.google.com/ServiceLogin?sacu=1&hl=en
    
    page.open('https://accounts.google.com/AddSession?sacu=1&hl=en', function(status) {
              if(status !== 'success') {
                fail('Test 4 failed - Unable to access google sign-in URL');
              } else {
                setTimeout(google1,pageTimeOut);
              }
    });
}

function google1() {
    setText('//*[@id=\"Email\"]','hp.mobility.usps.dev@gmail.com');
    clickOn('//*[@id=\"next\"]');
    setTimeout(google2,pageTimeOut);
}

function google2() {
//    console.log("using password '>&G`^r\\:#F5$XaJ' to sign into Google...");
    setText('//*[@id=\"Passwd\"]','>&G`^r\\:#F5$XaJ');
    clickOn('//*[@id=\"signIn\"]');
    setTimeout(google3,pageTimeOut); //5000
    
}

function google3() {
    console.log("going back to home page...");
    page.open('https://getclear18f.com', function(status) {
              if(status !== 'success') {
                fail('Google Sign-In failed - Unable to access home URL:'+status);
              } else {
                setTimeout(login,pageTimeOut);
              }
    });
    

}

function login() {
//    clickOn('//*[@id=\"nonAuthenticatedContainer\"]/div[2]/div/div[1]/a'); //click on Google single sign-on
    clickHref('//*[@id=\"nonAuthenticatedContainer\"]/div[2]/div/div[1]/a');
//    clickOn('//*[@id=\"nonAuthenticatedContainer\"]/div[2]/div/div[1]/div/div/div/div/div/svg');
    setTimeout(test5,pageTimeOut);
}

function test5() {
//    setTimeout(successful,1500); return;
    logTest(true);
    startTest("Test 5 - repeat adding irritant test (Cinnamic alcohol) but see that it exists in user profile");
    //checkBox('//*[@id=\"Aluminum\"]'); //check ALUMINUM
    clickOn('//*[@id=\"Cinnamic alcohol\"]'); //check Cinnamic alcohol
    clickOn('//*[@id=\"searchButton\"]'); //do the search!
    setTimeout(test5b,pageTimeOut); //6000
}

function test5b() {
//    setTimeout(successful,1500); return;
    var ret=elByXPath('//*[@id=\"title\"]/h3');
    if(ret==false || ret.indexOf("Search Result")==-1) {
        fail("Automated test 5 failed! Couldn't get search results for the irritant Cinnamic alcohol");
    }
    clickOn("//*[contains(@class,'do-not-buy-checkbox')]")
    //add products to profile
    clickOn('//*[@id=\"submit-do-not-buy-list\"]');
    setTimeout(test5c,pageTimeOut);
}

function test5c() {
    console.log("going to user's profile...");
    page.evaluate(function() {
                  document.location.href="https://getclear18f.com/#profile";
                  });
    setTimeout(test6,pageTimeOut);
    /*   page.open('https://getclear18f.com/'+encodeURIComponent('#')+'profile', function(status) {
     if(status !== 'success') {
     fail('Unable to access profile URL:'+status);
     } else {
     setTimeout(test5c,1400);
     }
     });*/

}

function test6() {
    var res=elByXPath("//*[contains(@class,' do-not-buy')]");
    if(res==false) {
        fail("Automated test 5 failed, Did not see product in Do-Not-Buy list that was added to profile!");
    }
    
    logTest(true);
    startTest("Test 6 - click on select irritants in user profile, then on popup click on Essential Oils tab and verify all oils are there (basil, etc)");
    var path='/html/body/div/section[1]/div/div[1]/div[2]/div[2]/div[4]/div/a';
    var ret=elByXPath(path);
    if(ret!=='Select Irritants') {
        fail("Automated test 6 failed - couldn't find Search Irritants link");
    }
    page.evaluate(function(path) { //get irritants popup
                  document.location.href=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().href;
                  },path);
    setTimeout(test6b,pageTimeOut);
    
}

function test6b() {
    //TODO: Need to click oils and check for basil here!!
//    page.render("popup.png");
    
    logTest(true);
    startTest("Test 7 - search for 'cream' click on first item in results, and see that we get to prod details page");
    console.log("Returning home...");
    page.open('https://getclear18f.com/', function(status) {
            if(status !== 'success') {
                fail('Test 7 failed - Unable to access home page URL:'+status);
            } else {
                setTimeout(test7,pageTimeOut);
            }
    });
}


function test7() {
    var res=setText('//*[@id=\"searchTextBox\"]','cream');
    if(res==false) {
        fail("Automated test 7 failed!, Couldn't find home search box!");
    }
    clickOn('//*[@id=\"searchButton\"]'); //search!
    setTimeout(test7b,pageTimeOut);
}

function test7b() {
    //*[@id="results-container"]/li[1]/div/div[2]/div[2]/div/a
    var path = '//*[@id=\"results-container\"]/li[1]/div/div[2]/div[2]/div/a'; //*[contains(@class,'search-item-name')]
    var ret=elByXPath('//*[@id=\"title\"]/h3');
    if(ret==false || ret.indexOf("Search Result")==-1) {
        fail("Automated test 7 failed!, Couldn't get search results for 'cream'");
    }
    ret=elByXPath(path);
    if(ret==false) {
        fail("Automated test 7 failed!, Couldn't find first search result");
    }
    page.evaluate(function(path) {
                  document.location.href=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().href;
                  },path);
    setTimeout(test7c,pageTimeOut);
}

function test7c() {
    var path='/html/body/div/section[1]/div/div/div[3]/div[1]/div[3]/div/a';
    var ret=elByXPath(path); //check for return to search results
    if(ret==false) {
        fail("Automated test 7 failed!, Did not make it to product detail page!");
    }
    if(ret.indexOf("Return")==-1||ret.indexOf("Search Results")==-1) {
        fail("Automated test 7 failed!, Did not make it to product detail page!");
    }
    logTest(true);
    startTest("Test 8 - returning to search results page via link on product detail page");
    page.evaluate(function(path) {
                  document.location.href=document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().href;
                  },path);
    setTimeout(test8,pageTimeOut);
}

function test8() {
    var path = '//*[@id=\"results-container\"]/li[1]/div/div[2]/div[2]/div/a';
    var ret=elByXPath('//*[@id=\"title\"]/h3');
    if(ret==false || ret.indexOf("Search Result")==-1) {
        fail("Automated test 8 failed!, Couldn't return to search results!");
    }
    console.log("Returning home again...");
    page.open('https://getclear18f.com/', function(status) {
            if(status !== 'success') {
                fail('Test 8 failed - Unable to access home page URL:'+status);
            } else {
                setTimeout(test9,pageTimeOut);
            }
    });
}

function test9() {
    logTest(true);
    startTest("Test 9 - log out, check to see that Google sign-in button is back and that Sign Out should be hidden");
    console.log("logging out...");
    var path='//*[@id=\"authenticatedContainer\"]/div[5]/div/div[2]/a';
    var ret=elByXPath(path);
    if(ret==false) {
        fail("Automated test 9 failed, Couldn't find logout button!");
    }
    console.log("Found logout button and it reads: "+ret);
    clickHref(path);
    setTimeout(test9b,pageTimeOut);
}

function test9b()
{
    //*[@id="nonAuthenticatedContainer"]
    var path='//*[@id=\"nonAuthenticatedContainer\"]';
    var ret=elByXPath(path);
    if(ret==false) {
        fail("Automated test 9 failed, Didn't see 'Sign In' label after logging out!");
    }
    var elStyle=page.evaluate(function(path) {
        var el = document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext();
        return el.style.display;
    },path);
//    console.log("*** Found Sign In div and it reads: "+ret+" ,style.display="+elStyle);
    if(elStyle!=='none') {
        fail("Automated test 9 failed!, Sign Out button was not hidden even though user has been logged out!");
    }
    setTimeout(successful,100); //call it good!
}

function successful() {
    logTest(true);
    writeReport('0');
    console.log("All tests passed! ("+(new Date()-startTime)/1000+" sec)");
    page.render("thepage.png");
    phantom.exit(0);
}

test1(); //let the testing begin!


