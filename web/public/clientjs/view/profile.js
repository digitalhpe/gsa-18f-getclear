var selectedIrritants = [];

GC.View.profile = Backbone.Marionette.ItemView.extend({
    template: GC.Template.profile,
    events: {
        "click #statusUpdate": "viewStatusUpdate",
        "click #saveIrritants": "saveIrritants",
        "click #searchButton": "performSearch",
        "change #select-all": "selectAllIrritants"
    },
    viewStatusUpdate: function () {
        GC.Data.View.Status = {};
        GC.Router.navigate("#profile", {trigger: true, replace: false});
    },
    saveIrritants: function () {
        GC.Data.app.user.knownIrritantsList = selectedIrritants;
        selectedIrritants = [];

        GC.WebServiceCalls.updateProfile(function (data, status) {
            console.log("Data:" + JSON.stringify(data));
        }, function (data) {
            console.log(data.responseText);
        });

        $('#irritantModal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        GC.Layout.content.show(new GC.View.profile());
    },
    performSearch: function () {
        GC.Data.app.searchTerm = "";
        GC.Router.navigate("#search_results", {trigger: true, replace: false});
    },
    selectAllIrritants: function () {
        if ($('#select-all').is(":checked")) {
            $('.knownIrritant').each(function (i, obj) {
                $(obj).addClass("btn-primary");
                $(obj).removeClass("btn-default");
                GC.Data.app.selectedIrritants.push(obj.id);
            });
            $('#searchButton').prop('disabled', false);
        } else {
            $('.knownIrritant').each(function (i, obj) {
                $(obj).addClass("btn-default");
                $(obj).removeClass("btn-primary");
                GC.Data.app.selectedIrritants = [];
            });
            $('#searchButton').prop('disabled', true);
        }
    },
    render: function () {

        GC.Utils.setGoogleAuthListener(function () {
                GC.Layout.content.show(new GC.View.profile());
            }
        );

        if (!GC.Data.app.irritants) {
            GC.WebServiceCalls.getIrritants(function (data, status) {
                GC.Data.app.irritants = data.irritants;
                GC.Router.navigate("#profile", {trigger: true, replace: false});
            }, function (data) {
                console.log(data.responseText);
            });
        }

        this.$el.html(this.template(GC.Data));
        return this;
    },
    onShow: function () {
        $("#irritantTab a").click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $(".irritant-check").change(function () {
            var irritantName = this.id;

            if (this.checked) {
                selectedIrritants.push(irritantName);
            } else {
                var index = selectedIrritants.indexOf(irritantName);

                if (index > -1) {
                    selectedIrritants.splice(index, 1);
                }
            }
        });

        $(".knownIrritant").click(function () {
            var irritantName = this.id;

            var index = GC.Data.app.selectedIrritants.indexOf(irritantName);

            if (index > -1) {
                GC.Data.app.selectedIrritants.splice(index, 1);
                $(this).addClass("btn-default");
                $(this).removeClass("btn-primary");
            } else {
                GC.Data.app.selectedIrritants.push(irritantName);
                $(this).addClass("btn-primary");
                $(this).removeClass("btn-default");
            }

            setSearchButtonState();
        });

        // loop through all know irritants and set the button click state
        $('.knownIrritant').each(function (i, obj) {
            var index = GC.Data.app.selectedIrritants.indexOf(obj.id);
            if (index > -1) {
                $(obj).addClass("btn-primary");
                $(obj).removeClass("btn-default");
            } else {
                $(obj).addClass("btn-default");
                $(obj).removeClass("btn-primary");
            }
        });

        // loop through all irritants in the modal and set their state accordingly...
        selectedIrritants = [];
        $('.irritant-check').each(function (i, obj) {
            var index = GC.Data.app.user.knownIrritantsList.indexOf(obj.id);

            if (index > -1) {
                $(obj).prop('checked', true);
                selectedIrritants.push(obj.id);
            }
        });

        setSearchButtonState();

        if (GC.Data.app.selectedIrritants.length === GC.Data.app.user.knownIrritantsList.length) {
            $('#select-all').prop('checked', true);
        } else {
            $('#select-all').prop('checked', false);
        }

        $('.product_image').each(function (i, obj) {
            if (obj.src.endsWith('/')) {
                obj.src = '/img/no_image_sm.png';
            }
        });
    }
});

function setSearchButtonState() {
    if (GC.Data.app.selectedIrritants.length > 0) {
        $('#searchButton').prop('disabled', false);
    } else {
        $('#searchButton').prop('disabled', true);
    }
}

function logoutGoHome() {
    GC.Utils.googleSignOut();
    GC.Router.navigate("#home", {trigger: true, replace: false});
}