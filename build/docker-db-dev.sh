#!/bin/bash

#Database image
running=$(sudo docker ps -a -q -f 'name=gsa-dev-db' -f 'event=start')

for i in $running;
do
	echo "Stopping DB Container"
	sudo docker stop $i
	echo "Removing DB Container"
	sudo docker rm $i
done 

#Build a new image
sudo docker build -t gsa-mongodb database/.

#Start the image
sudo docker run --name gsa-dev-db -d gsa-mongodb

#Populate the DB with initial datas
sudo docker exec gsa-dev-db mongo /data/dbsetup.js
	
