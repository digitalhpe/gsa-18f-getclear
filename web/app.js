var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var Q = require('q');

var webApp = require('./routes/index');
var users = require('./routes/users');
var factual = require('./routes/factual');
var productinfo = require('./routes/productinfo');
var ingredientinfo = require('./routes/ingredientinfo');
var irritants = require('./routes/irritants');
var profile = require('./routes/profile');
var oneproduct = require('./routes/oneproduct');
var compression = require('compression');

var app = express();

app.use(compression());

app.use(express.static(path.join(__dirname, 'public'), { maxAge: 2592000000 }));

var testConfig = false;
if (process.env.NODE_ENV === 'test') {
    testConfig = true;
    console.log('Unit Test Mode Enabled');
}
else if (process.argv[2] === '80') {
    app.port = 80;
}

var dataLayer;
if (testConfig) {
    dataLayer = require('./test/mockDataLayer.js');
} else {
    dataLayer = require('./data/data.js');
}

var utils = require('./start/common.js');
app.locals.dataLayer = dataLayer;
app.locals.utils = utils;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', webApp);
app.use('/users', users);
app.use('/api/getFactualID', factual);
app.use('/api/getProductInfo', productinfo);
app.use('/api/getIngredientInfo', ingredientinfo);
app.use('/api/getIrritants', irritants);
app.use('/api/getProfile', profile);
app.use('/api/updateProfile', profile);
app.use('/api/getProductUPC', oneproduct);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
