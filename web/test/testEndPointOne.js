var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('Placeholder test', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});