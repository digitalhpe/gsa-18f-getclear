/**
 * Created by jimpeters on 6/28/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testgetProfile', function () {
    it ('should return 400 for missing email_addr', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProfile')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter missing');

                done();
            });
    });

    it ('should return 400 for empty email_addr', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProfile?email_addr')
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('get parameter empty');

                done();
            });
    });

    it ('should return 404 for not found', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProfile?email_addr=jimmmm.peters@hp.com')
            .expect('Content-Type', /json/)
            .expect(404)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Not Found');

                done();
            });
    });

    it ('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getProfile?email_addr=jim.peters@hp.com')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                done();
            });
    });
});