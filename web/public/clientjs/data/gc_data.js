var GC_Data = GC_Data || {};
GC_Data["app"] =
{
    "app-name": "Get Clear",
    "user": {
        "authenticated": false,
        "name": "",
        "email": "",
        "imageUrl": "",
        "id": "",
        "idToken": "",
        "knownIrritantsList": [],
        "doNotBuyList": []
    },
    "irritants": null,
    "searchTerm": "",
    "selectedIrritants": []
};