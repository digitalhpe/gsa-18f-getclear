#!/bin/bash

#Database image
running=$(sudo docker ps -a -q -f 'name=gsa-prod-db' -f 'event=start')

for i in $running;
do
	echo "Stopping DB Container"
	sudo docker stop $i
	echo "Removing DB Container"
	sudo docker rm $i
done 

#Build a new image
sudo docker build -t gsa-mongodb database/.

#Start the image
sudo docker run --name gsa-prod-db -d gsa-mongodb

#Populate the DB with initial datas
sudo docker exec gsa-prod-db mongo /data/dbsetup.js