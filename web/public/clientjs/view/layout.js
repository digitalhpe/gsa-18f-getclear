GC.View.layout = Marionette.LayoutView.extend({
    el: "div.container-fluid",
    template: GC.Template.layout,
    regions: {
        header: "header",
        footer: "footer",
        content: "section.content",
        controls: "section.controls",
        modal: "section.modal-container"
    }
});
