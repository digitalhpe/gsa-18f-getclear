/**
 * Created by jimpeters on 6/23/15.
 */

var baseRoute = module.exports = {};

baseRoute.handleError = function(err, res) {
    if (err) {
        if (err.message === 'Not Found'){
            res.status(404).json({"result": "error", "reason": err.message});
        } else if (err.message === 'Server error') {
            res.status(500).json({"result": "error", "reason": err.message});
        } else if (err.code || err.reason) {
            res.status(500).json({"result": "error", "code": err.code, "reason": err.message});
        } else {
            res.status(500).json({"result": "error", "reason": err});
        }
    }
};
