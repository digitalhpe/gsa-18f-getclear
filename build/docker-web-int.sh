#!/bin/bash

# Remove running instances
running=$(sudo docker ps -a -q -f 'name=gsa-web')

for i in $running;
do
	echo "Stopping Web Container"
	sudo docker stop $i
	echo "Removing Web Container"
	sudo docker rm $i
done  

#Create the new image
sudo docker build -t gsa-nodejs web/.

#Start a container with this image
sudo docker run --name gsa-web --link gsa-prod-db:db -e "NODE_ENV=integration" -p 80:3000 -d gsa-nodejs