#!/bin/bash

# Remove running instances
running=$(sudo docker ps -a -q -f 'name=gsa-redirect-80')

for i in $running;
do
	echo "Stopping Web Container"
	sudo docker stop $i
	echo "Removing Web Container"
	sudo docker rm $i
done  

#Create the new image
sudo docker build -t gsa-redirect redirect/.

#Start a container with this image
sudo docker run --name gsa-redirect-80 -p 80:4000 -d gsa-redirect