/**
 * Created by andrjone on 6/24/2015.
 */
var utils = utils || {};

var profileLoadedListener;

var CLIENT_ID = '931334585226-opain5mee8cpk5dka36n457hv39qm5j6.apps.googleusercontent.com';
var apiKey = 'AIzaSyAcBtXC6m3ZbEwPoXyMkY4FdlceU2sajX0';
var scopes = 'https://www.googleapis.com/auth/plus.me';

// Triggered by a page load
function handleClientLoad() {
    gapi.client.setApiKey(apiKey);
    checkAuthenticationStatus();
}

// Triggered by a G+ button click
function handleAuthClick() {
    gapi.auth.signIn({
        clientid: CLIENT_ID,
        cookiepolicy: 'single_host_origin',
        scope: scopes,
        callback: handleAuthResult
    });
}

utils.googleSignOut = function signOut() {
    var result = gapi.auth.signOut();
    GC.Data.app.user.authenticated = false;
    profileLoadedListener();
};

function checkAuthenticationStatus() {
    gapi.client.load('plus', 'v1', function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            if (resp.code !== 200) {
                // User isn't authenticated
                profileLoadedListener();
            } else {
                // User is authenticated
                id = resp.id;
                email = resp.emails[0].value;
                name = resp.displayName;
                imageUrl = resp.image.url;

                GC.Data.app.user.id = id;
                GC.Data.app.user.email = email;
                GC.Data.app.user.name = name;
                GC.Data.app.user.imageUrl = imageUrl.replace('sz=50', 'sz=200');

                if (id > 0) {
                    GC.WebServiceCalls.getProfile(email, function (data, status) {
                        console.log("Data:" + JSON.stringify(data));
                        GC.Data.app.user.knownIrritantsList = data[0].ingredients;
                        GC.Data.app.user.doNotBuyList = data[0].products;
                        profileLoadedListener();
                    }, function (data) {
                        console.log(data.responseText);

                        GC.WebServiceCalls.updateProfile(function (data, status) {
                            console.log("Data:" + JSON.stringify(data));
                        }, function (data) {
                            console.log(data.responseText);
                        });
                    });
                } else {
                    GC.Data.app.user.knownIrritantsList = [];
                    GC.Data.app.user.doNotBuyList = [];
                }
                profileLoadedListener();
            }
        });
    });
}

function handleAuthResult(authResult) {
    if (authResult.status.signed_in) {
        GC.Data.app.user.authenticated = true;
        loadProfile();
    } else {
        GC.Data.app.user.authenticated = false;
        profileLoadedListener();
    }
}

function loadProfile() {
    gapi.client.load('plus', 'v1', function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            var id = -1;
            var email = '';
            var name = '';
            var imageUrl = '';

            if (resp != null) {
                id = resp.id;
                email = resp.emails[0].value;
                name = resp.displayName;
                imageUrl = resp.image.url;

                GC.Data.app.user.id = id;
                GC.Data.app.user.email = email;
                GC.Data.app.user.name = name;
                GC.Data.app.user.imageUrl = imageUrl.replace('sz=50', 'sz=200');

                if (id > 0) {
                    GC.WebServiceCalls.getProfile(email, function (data, status) {
                        console.log("Data:" + JSON.stringify(data));
                        GC.Data.app.user.knownIrritantsList = data[0].ingredients;
                        GC.Data.app.user.doNotBuyList = data[0].products;
                        profileLoadedListener();
                    }, function (data) {
                        console.log(data.responseText);

                        GC.WebServiceCalls.updateProfile(function (data, status) {
                            console.log("Data:" + JSON.stringify(data));
                        }, function (data) {
                            console.log(data.responseText);
                        });
                    });
                } else {
                    GC.Data.app.user.knownIrritantsList = [];
                    GC.Data.app.user.doNotBuyList = [];
                }
                profileLoadedListener();
            }
        });
    });
}

function handleClientLoadX() {
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: CLIENT_ID,
            scope: 'profile'
        });

        auth2.isSignedIn.listen(function listener(value) {
            console.log("Login state change: " + value);
        });

        // Listen for changes to current user.
        auth2.currentUser.listen(function userChanged(googleUser) {
            var profile = googleUser.getBasicProfile();
            var id = -1;
            var idToken = '';
            var email = '';
            var name = '';
            var imageUrl = '';

            if (profile) {
                idToken = googleUser.getAuthResponse().id_token;
                id = profile.getId();
                email = profile.getEmail();
                name = profile.getName();
                imageUrl = profile.getImageUrl();

                GC.Data.app.user.id = id;
                GC.Data.app.user.idToken = idToken;
                GC.Data.app.user.email = email;
                GC.Data.app.user.name = name;
                GC.Data.app.user.imageUrl = imageUrl;

                if (id > 0) {
                    GC.WebServiceCalls.getProfile(email, function (data, status) {
                        console.log("Data:" + JSON.stringify(data));
                        GC.Data.app.user.knownIrritantsList = data[0].ingredients;
                        GC.Data.app.user.doNotBuyList = data[0].products;
                        if (profileLoadedListener) {
                            profileLoadedListener();
                        }
                    }, function (data) {
                        console.log(data.responseText);

                        GC.WebServiceCalls.updateProfile(function (data, status) {
                            console.log("Data:" + JSON.stringify(data));
                            profileLoadedListener();
                        }, function (data) {
                            console.log(data.responseText);
                        });
                    });
                } else {
                    GC.Data.app.user.knownIrritantsList = [];
                    GC.Data.app.user.doNotBuyList = [];
                }
            } else if (profileLoadedListener) {
                profileLoadedListener();
            }

            GC.Data.app.user.authenticated = auth2.isSignedIn.get();
        });
    });
}

utils.setGoogleAuthListener = function (profileLoaded) {
    profileLoadedListener = profileLoaded;
};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};