var conn = new Mongo();
var db = conn.getDB("gsa");

db.irritant.drop();

db.irritant.insert({"category": "irritantsMisc", "irritant": "Aluminum"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Salicylic Acid"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Salicylate"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Glycolic Acid"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Alcohol"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Ammonia"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Menthol"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Peroxide"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Capsaicin"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Benzocaine"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Hydrocortisone"});
db.irritant.insert({"category": "irritantsMisc", "irritant": "Neomycin"});
db.irritant.insert({"category": "irritantsFragrance", "irritant":"Cinnamic alcohol"});
db.irritant.insert({"category": "irritantsFragrance", "irritant":"Eugenol"});
db.irritant.insert({"category": "irritantsFragrance", "irritant":"Geraniol"});
db.irritant.insert({"category": "irritantsFragrance", "irritant":"Hydroxycitronellal"});
db.irritant.insert({"category": "irritantsEmollient", "irritant":"Lanolin"});
db.irritant.insert({"category": "irritantsEmollient", "irritant":"Coconut Butter"});
db.irritant.insert({"category": "irritantsEmollient", "irritant":"Cocoa Butter"});
db.irritant.insert({"category": "irritantsEmollient", "irritant":"Isopropyl Palmitate"});
db.irritant.insert({"category": "irritantsEmollient", "irritant":"Myristyl Lactate"});
db.irritant.insert({"category": "irritantsSulfate", "irritant":"Sodium Laureth"});
db.irritant.insert({"category": "irritantsSulfate", "irritant":"Sodium Laurel"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Basil"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Benzoin"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Cassia"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Clove"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Ginger"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Lemon"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Lemon Verbena"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Peppermint"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Pimento Berry"});
db.irritant.insert({"category": "irritantsEssentialOil", "irritant":"Pine"});

db.profile.drop();

