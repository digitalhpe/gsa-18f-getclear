/**
 * Created by andrjone on 6/23/2015.
 */
/* Start Application */
$(document).ready(function () {

    var App = new GC.App({container: "div.container-fluid"});

    App.on('start', function () {
        GC.Layout = new GC.View.layout();
        GC.Layout.render();
        GC.Layout.header.show(new GC.View.header());
        GC.Layout.footer.show(new GC.View.footer());

        GC.Router = new GC.Router();

        Backbone.history.start();
    });

    App.start();
});
