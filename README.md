# HP's 18F ADS Prototype Development Approach #

## Prototype ##
### URL ###
[https://getclear18f.com](https://getclear18f.com/ "GetClear Prototype")

### Installation Instructions ###
Detailed installation instructions are located in the [Install.md](https://bitbucket.org/hpuspsmobility/gsa-18f-getclear/src/7980d2eae0e3b10948d7c2be9b6793c7c2779f4d/docs/Install.md?at=master) file.

## Approach Overview ##

Over the past 5 years, HP has been acquiring top-tier talent in the areas of web/mobile application development and agile delivery. When the 18F ADS RFQ was released, we quickly assembled and co-located a development team consisting of 8 individuals to deliver the response. Team roles included a Product Owner who had full authority and responsibility over the project, and Scrum Master who coordinated and facilitated the development process. All team members played cross-functional roles throughout the process.

Given the original requirements and scope, our team's mantra was “5 days from ideate to deploy!”. We immediately began analyzing the openFDA APIs and data. The entire team ideated on how we could utilize openFDA to provide a valuable service to the public. In order to constrain the subject and scope of the prototype, we decided to focus on a sub-set of the openFDA data, skin irritants. Our Product Owner came up with the "GetClear" name later in the week. 

Other up-front activities included collaborating on how to best tailor our agile scrum process to maximize productivity given the time frame to respond. Given the original schedule, we decided to work under daily Sprints and immediately began engaging stakeholders outside our team to act as our "users". Each day we held a morning stand-up/Sprint Planning meeting and an end-of-day users/stakeholders meeting. At the user meetings we shared wireframes, mock-ups and live demos as the application progressed throughout the week. Our users provided feedback on the design and performed usability testing once the site was online. This input was extremely valuable and was frequently incorporated into the application.

We setup our [Jira cloud instance](https://bitbucket.org/hpuspsmobility/gsa-18f-getclear/src/7980d2eae0e3b10948d7c2be9b6793c7c2779f4d/docs/agile/?at=master) and populated it with our product backlog of user stories. Early stories included activities such as infrastructure and environment setup, preliminary design activities such as site maps and wireframes, and test case development. We also investigated other public repositories that would be good candidates to supplement the product data we were retrieving from openFDA. We decided on [factual](http://www.factual.com/) and used the UPC and product names from products retrieved from openFDA for the mashup. One of the really useful pieces of data from factual were product images which greatly enhanced the UX/UI for GetClear search results. 

The GetClear prototype was developed as a responsive web application. We executed tests across a variety of form factors including multiple phone, tablet and desktop browser window sizes. GetClear was built using dozens of integrated open source tools spanning the front-end, back-end, and DevOps. A list of some of the tools are described in the section below.

The team stayed focused and used our retrospectives each morning to quickly adapt and make improvements to our approach. By day 4 the team had completed most of the programming backlog items and was performing UI cleanup, bug fixing, and documentation. 

Our team really enjoyed responding to this RFQ and hope we can support 18F in the future under this new ADS BPA.

## Tools Used ##
### Infrastructure ###
- **BitBucket** - git-based source control
- **Node.js** - JavaScript engine and library
- **Express** - back-end web framework, request endpoints, cookies
- **Monk** - entity schemata 
- **Q** - asynchronous functions, return value and exception handling
- **Mocha** - JavaScript testing
- **Grunt** - automation, unit testing
- **PhantomJS** - browser automation, web testing
- **Jenkins** - continuous integration
- **Docker** - containerized environments
- **npm** - node package and dependency management
- **SonarQube** - code quality
- **MongoDB** - data store
- **HP Helion** - IaaS hosting

### Application ###
- **Bootstrap** - responsive web UI framework
- **jQuery** - JavaScript library for DOM manipulation
- **Backbone.js** - SPA framework
- **Marionnette.js** - abstraction helpers for Backbone
- **Handlebars.js** - templating framework
- **Bower** - client-side dependency management

### Process ###
- **Jira** - agile project management
- **Confluence** - team collaboration

## Key Approach Considerations ##

### Human-Centered Design ###
Following the [U.S. Digital Services Playbook](https://playbook.cio.gov/) tenets of understanding people's needs and addressing the whole experience intuitively, we employed the following human-centered design techniques to create GetClear:

- **Personas**
We created three distinct Personas for our GetClear prototype: a patient with extensive understanding of allergens, a student who just wants product information, and a parent who is learning about allergens for the first time. Each Persona embodied not just a common use case scenario, but also a potential user demographic. 

- **Journey Maps**
Once the Personas were created, we mapped out how each one would accomplish a certain task. This helped us understand the current situation of the audience and how our prototype might benefit them. We identified tasks the audience would be doing the majority of the time and created one Journey Map for each Persona, guiding them through one of the three main uses of the site: performing a search via the profile, looking up a specific product, and researching a common allergen.  

- **User Tests**
Armed with the knowledge gained from Personas and Journey Maps, the team set about making a product specifically catered to the needs of its audience. Recognizing the importance of having SMEs and users verify the prototype, we had "users" from outside the development team test it and we made adjustments to the site based on that feedback. 

Our Visual Designer created a [GetClear Design Style Guide](https://bytebucket.org/hpuspsmobility/gsa-18f-getclear/raw/7980d2eae0e3b10948d7c2be9b6793c7c2779f4d/docs/design/GetClear_Styleguide.pdf) for the developers to use when coding the prototype.

### IaaS/PaaS ###
The GetClear application was deployed using the HP Helion IaaS platform.

### Unit Testing ###
Our team uses Mocha, a JavaScript unit test framework, to write automated unit tests which are executed by a Grunt task on Jenkins. Sample screen shots of our unit tests are posted in the [Test](https://bitbucket.org/hpuspsmobility/gsa-18f-getclear/src/7980d2eae0e3b10948d7c2be9b6793c7c2779f4d/docs/test/?at=master) folder.

### DevOps ###
This project utilized our existing Jenkins-based continuous integration system. This application used many build scripts to fulfill all the necessary build tasks, including building the software, running and documenting tests, building Docker containers, and deploying the containers. The build scripts are found in the [build directory](https://bitbucket.org/hpuspsmobility/gsa-18f-getclear/src/7980d2eae0e3b10948d7c2be9b6793c7c2779f4d/build/?at=master) in the root of the repository.

### Configuration Management ###
The GetClear application utilized the [node-config library](https://github.com/lorenwest/node-config) to manage environment-specific parameters for server-side code, such as connection strings and API keys. For client-side configuration the project utilized [grunt-env](https://github.com/jsoverson/grunt-env).

### Continuous Monitoring ###
The Continuous Monitoring Solution includes on-going automated Tenable Nessus scans to identify, track and review security vulnerabilities, and conduct mitigation. Nessus scans are performed remotely via the network using the leveraged Tenable Nessus solution. The appropriate IP addresses, and system and application credentials are entered into the Tenable Nessus tool for fully authenticated network/system/and application scans to be conducted on a regular basis. 

We use HP Fortify to conduct secure code analysis to ensure ongoing identification and mitigation of security vulnerabilities early on and throughout the software development life cycle. HP also utilizes WebInspect to scan for any web security issues, and implement corrective measures.

The site/network and application are monitored 24x7 using the ArcSight SIEM and HP Security Operations Center (SOC). The system, network and application logs are fed into the leveraged ArcSight SIEM, monitored, managed and controlled by the network and security engineering team within the HP SOC.

### Containerization ###
The GetClear application is deployed using [Docker](http://www.docker.com) containers. The containers are generated and deployed by the Jenkins-based CI system.

### External APIs ###
GetClear is a single-page web application utilizing it's own RESTful API built with Node.JS. We utilized the following APIs for the GetClear prototype:


- **[Open FDA API](https://open.fda.gov/api/reference/)**
 An Elasticsearch-based External link icon API External link icon that serves public FDA data about three entities: Drugs, Devices and Foods. Each of these has one or more endpoints, which serve unique data—such as data about recall enforcement reports, or adverse events. Every API query must go through one endpoint for one kind of data.

- **[factual CPG Global Products Data API](http://developer.factual.com/working-with-factual-products/)**
[factual product nutrition](http://api.v3.factual.com/t/products-cpg-nutrition) provides access to core product attributes plus nutrition and ingredients attributes.
The Products table can be read using factual’s Read API call, and supports the row filters and search filters. Results are returned in JSON.

- **[Google+ API](https://developers.google.com/identity/sign-in/web/sign-in)**
Sign-in for websites to authenticate users and pull user profile information. We use the Google+ API to authenticate users and pull the user's name, email, and photo from Google.


### Open Source ###
All tools, frameworks and platforms used to create our GetClear prototype are open source, free and openly licensed. Examples of many of the open source licenses used can be found in the [Licenses folder](https://bitbucket.org/hpuspsmobility/gsa-18f-getclear/src/7980d2eae0e3b10948d7c2be9b6793c7c2779f4d/docs/licenses/?at=master).

### Reference Material ###
For increased visibility into our development process, many photos and screen captures were captured and may be viewed in the [Docs folder](https://bitbucket.org/hpuspsmobility/gsa-18f-getclear/src/7980d2eae0e3/docs/?at=master).


## Footnotes ##
- We interpret factual to meet the open license requirements based on their [terms of service](http://www.factual.com/tos).
- The daily limit for look-ups on the factual free license is 500.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             