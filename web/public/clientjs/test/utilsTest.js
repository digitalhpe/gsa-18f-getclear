var should = require('should');
var assert = require('assert');
var request = require('supertest');
var Utils = require('../js/utils.js');

describe('UtilsTests', function () {
    describe('endsWith()', function () {
        it('should return true when the string ends with the given character(s)', function () {
            "testpath/".endsWith('/').should.equal(true);
            "testpath".endsWith('h').should.equal(true);
            "testpath/ ".endsWith(' ').should.equal(true);
            " ".endsWith(' ').should.equal(true);
            "testpath/".endsWith('testpath/').should.equal(true);
            "testpath/".endsWith('q').should.equal(false);
        })
    })
});