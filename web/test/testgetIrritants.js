/**
 * Created by jimpeters on 6/28/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testgetIrritants', function () {
    it ('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request('http://localhost:' + port)
            .get('/api/getIrritants')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});
