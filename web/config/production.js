/**
 * Created by trux on 6/24/15.
 */

module.exports = {
    ssl: true,

    key: '/ssl/getclear.key',
    cert: '/ssl/getclear.crt',
    ca: '/ssl/gd_bundle-g2-g1.crt',

    //or use DB_PORT with tcp:// prepended to the IP address and port
    connectionString: process.env.DB_PORT_27017_TCP_ADDR + ":27017/gsa",

    factualKey: "",
    factualSecret: ""

}