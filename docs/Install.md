# Installing Get Clear
## Server Pre-Requisites
The web application was built using Node.js and MongoDb. These are both deployed using Docker containers, so there is no need to install the server software if deploying using Docker containers. The application will need a [Docker Server](http://www.docker.com) as a deployment destination.
## Build Pre-Requisites
To build the software, the following pre-requisites need to be installed on the computer building the software:

- [Docker](http://www.docker.com)

### Tests
In order to run the unit tests and/or integration tests, the computer needs the following frameworks installed:

- [Node.js](https://nodejs.org)
- [npm](https://www.npmjs.com/) (typically installed with Node.js)
- [Grunt](http://gruntjs.com)
- [Bower](http://bower.io/)
- [PhantomJS](http://phantomjs.org/)

## Build the Software
The *build* subdirectory of the repository contains scripts for building the software. These scripts assume that the software is built on the same server that is running Docker. To deploy the software, run the scripts in this order:

1. **docker-db-prod.sh**: This script creates a Docker container with MongoDB installed, and runs a database setup script to populate the database with static data needed by the application. The script starts the container as well. This container must be built and running before the web application container.
2. **docker-web-int.sh**: This script builds a Docker container with the web application. Additional build steps occur when the container is started. The script starts the container on port 80. This container is linked to the database container.

### Helpful Information
When pulling the scripts from the repository, you may need to make them executable:

`sudo chmod +x ./build/docker-db-prod.sh`

`sudo chmod +x ./build/docker-web-int.sh`

Both scripts assume your working directory is the root of the repository. So you would execute the script from the repository root: 

`./build/docker-db-prod.sh`

###Other Scripts

- **GSA-18F-get-clear-web.sh**: This script builds and tests the software without deployment to a Docker container.
- **docker-web-ssl.sh**: This script builds the TLS-based version of the software. It is identical to docker-web-nossl.sh except that is adds the certificates which live on the build server into the container and runs the app on port 443 instead of 80.
- **docker-web-redirect.sh**: This script deploys a small web application located in the *redirect* directory of the repository. This script listens for requests on port 80 and redirects them to port 443. This is not necessary unless deploying the application with TLS.

