/**
 * Created by jimpeters on 6/24/15.
 */
var Q = require('q');
var data = module.exports = {};

data.findActiveIngredient = function(ingredientString, callback) {
    if (ingredientString === 'somethingbad') {
        //Simulate not found
        callback(null, null);
        return;
    }

    if (ingredientString === 'somethingreallybad') {
        //Simulate server error
        var data = {
            "error": {
                "code": "SERVER_ERROR"
            }
        };

        callback(null, JSON.stringify(data));
        return;
    }

    var data =  {
        "meta": {
        "disclaimer": "openFDA is a beta research project and not for clinical use. While we make every effort to ensure that data is accurate, you should assume all results are unvalidated.",
            "license": "http://open.fda.gov/license",
            "last_updated": "2015-05-31",
            "results": {
            "skip": 0,
                "limit": 1,
                "total": 95
        }
    },
        "results": [
        {
            "set_id": "0230a89a-350d-4f9b-9b05-75ef92ab5254",
            "indications_and_usage": [
                "Uses - helps prevent sunburn - if used as directed with other sun protection measures (see Directions), decreases the risk of skin cancer and early skin aging caused by the sun"
            ],
            "stop_use": [
                "Stop use and ask a doctor if rash occurs"
            ],
            "keep_out_of_reach_of_children": [
                "Keep out of reach of children. If swallowed, get medical help or contact a Poison Control Center right away."
            ],
            "dosage_and_administration": [
                "Directions For sunscreen use: ● apply generously 15 minutes before sun exposure ● reapply: ● after 80 minutes of swimming or sweating ● immediately after towel drying ● at least every 2 hours ● Sun Protection Measures. Spending time in the sun increases your risk of skin cancer and early skin aging. To decrease this risk, regularly use a sunscreen with a Broad Spectrum SPF value of 15 or higher and other sun protection measures including: ● limit time in the sun, especially from 10 a.m. – 2 p.m. ● wear long-sleeved shirts, pants, hats, and sunglasses ● children under 6 months of age: Ask a doctor"
            ],
            "purpose": [
                "Purpose Sunscreen"
            ],
            "storage_and_handling": [
                "Other information protect the product in this container from excessive heat and direct sun"
            ],
            "do_not_use": [
                "Do not use on damaged or broken skin"
            ],
            "version": "2",
            "id": "9eef55b0-2c62-4e74-affe-60a53b0ffbb9",
            "package_label_principal_display_panel": [
                "image of label"
            ],
            "active_ingredient": [
                "Active ingredients Avobenzone 3% Homosalate 15% Octisalate 5% Octocrylene 5% Oxybenzone 6%"
            ],
            "inactive_ingredient": [
                "Inactive ingredients water, dimethicone, isododecane, styrene/acrylates copolymer, propanediol, glycerin, silica, isononyl isononanoate, inulin lauryl carbamate, nylon-12, caprylyl methicone, synthetic wax, poly C10-30 alkyl acrylate, PEG-8 laurate, stearyl alcohol, dimethiconol, triethanolamine, isoeugenol, fragrance, vitis vinifera (grape) fruit extract, phenoxyethanol, p-anisic acid, ammonium acryloyldimethyltaurate/steareth-25 methacrylate crosspolmer, chlorphenesin, disodium EDTA, tocopherol, sucrose tristearate, xanthan gum, polymethyl methacrylate"
            ],
            "@epoch": 1416004577.444619,
            "effective_time": "20140121",
            "openfda": {
                "spl_id": [
                    "9eef55b0-2c62-4e74-affe-60a53b0ffbb9"
                ],
                "product_ndc": [
                    "49967-325"
                ],
                "is_original_packager": [
                    true
                ],
                "route": [
                    "TOPICAL"
                ],
                "substance_name": [
                    "OCTISALATE",
                    "OXYBENZONE",
                    "HOMOSALATE",
                    "OCTOCRYLENE",
                    "AVOBENZONE"
                ],
                "spl_set_id": [
                    "0230a89a-350d-4f9b-9b05-75ef92ab5254"
                ],
                "package_ndc": [
                    "49967-325-01"
                ],
                "product_type": [
                    "HUMAN OTC DRUG"
                ],
                "upc": [
                    "0071249273258"
                ],
                "generic_name": [
                    "AVOBENZONE, HOMOSALATE, OCTISALATE, OCTOCRYLENE AND OXYBENZONE"
                ],
                "manufacturer_name": [
                    "L'Oreal USA Products Inc"
                ],
                "brand_name": [
                    "LOreal Paris Advanced Suncare Silky Sheer 50 Plus Broad Spectrum SPF 50 Plus Sunscreen"
                ],
                "application_number": [
                    "part352"
                ]
            },
            "spl_product_data_elements": [
                "LOreal Paris Advanced Suncare Silky Sheer 50 Plus Broad Spectrum SPF 50 Plus Sunscreen Avobenzone, Homosalate, Octisalate, Octocrylene and Oxybenzone AVOBENZONE AVOBENZONE HOMOSALATE HOMOSALATE OCTISALATE OCTISALATE OCTOCRYLENE OCTOCRYLENE OXYBENZONE OXYBENZONE"
            ],
            "when_using": [
                "When using this product keep out of eyes. Rinse with water to remove."
            ],
            "warnings": [
                "Warnings For external use only"
            ]
        }
    ]
    };

    callback(null, JSON.stringify(data));
};

data.findProduct = function(thisProduct, callback) {
    if (thisProduct === 'somethingbad') {
        //Simulate product  not found
        callback(null, null);
        return;
    }

    if (thisProduct === 'somethingreallybad') {
        //Simulate server error
        var data = {
            "error": {
                "code": "SERVER_ERROR"
            }
        };

        callback(null, JSON.stringify(data));
        return;
    }

    var data =  {
        "meta": {
            "disclaimer": "openFDA is a beta research project and not for clinical use. While we make every effort to ensure that data is accurate, you should assume all results are unvalidated.",
            "license": "http://open.fda.gov/license",
            "last_updated": "2015-05-31",
            "results": {
                "skip": 0,
                "limit": 25,
                "total": 12
            }
        },
        "results": [
            {
                "set_id": "0f5b2c4c-55f2-4140-b706-f260e2c96cbc",
                "indications_and_usage": [
                    "Uses: For temporary relief of the symptoms of dry lips"
                ],
                "keep_out_of_reach_of_children": [
                    "Keep out of reach of children. If swallowed, get medical help or contact a Poison Control Center right away."
                ],
                "dosage_and_administration": [
                    "Directions apply to lips not more than 3 to 4 times daily children under 2 years of age: ask a doctor"
                ],
                "purpose": [
                    "Purpose External Analgesic"
                ],
                "version": "3",
                "id": "b7f6fb80-3ead-46a1-97fe-0d63a224de6b",
                "package_label_principal_display_panel": [
                    "CARMEX original lip balm EXTERNAL ANALGESIC 7.5g (10210-0011-0)| CARMEX original lip balm EXTERNAL ANALGESIC 7.5g/Jar in Blister Pack (10210-0011-1) CARMEXExternalAnalgesic2 CARMEXExternalAnalgesic3 JarBlisterCard"
                ],
                "active_ingredient": [
                    "Active Ingredients Camphor 1.7% Menthol 0.7% Purpose External Analgesic"
                ],
                "inactive_ingredient": [
                    "Inactive Ingredients petrolatum, lanolin, cetyl esters, theobroma cacao (cocoa) seed butter, paraffin, beeswax, flavor, salicylic acid, phenol"
                ],
                "@epoch": 1415927453.475662,
                "effective_time": "20140814",
                "openfda": {
                    "spl_id": [
                        "b7f6fb80-3ead-46a1-97fe-0d63a224de6b"
                    ],
                    "product_ndc": [
                        "10210-0011"
                    ],
                    "rxcui": [
                        "1439969",
                        "1439974"
                    ],
                    "substance_name": [
                        "MENTHOL",
                        "CAMPHOR (NATURAL)"
                    ],
                    "spl_set_id": [
                        "0f5b2c4c-55f2-4140-b706-f260e2c96cbc"
                    ],
                    "product_type": [
                        "HUMAN OTC DRUG"
                    ],
                    "upc": [
                        "0083078011307"
                    ],
                    "manufacturer_name": [
                        "Carma Laboratories, Inc."
                    ],
                    "brand_name": [
                        "CARMEX original lip balm EXTERNAL ANALGESIC"
                    ],
                    "is_original_packager": [
                        true
                    ],
                    "route": [
                        "TOPICAL"
                    ],
                    "package_ndc": [
                        "10210-0011-0",
                        "10210-0011-1"
                    ],
                    "generic_name": [
                        "CAMPHOR (NATURAL), MENTHOL"
                    ],
                    "application_number": [
                        "part348"
                    ]
                },
                "spl_product_data_elements": [
                    "CARMEX original lip balm EXTERNAL ANALGESIC CAMPHOR (NATURAL), MENTHOL CAMPHOR (NATURAL) CAMPHOR (NATURAL) MENTHOL MENTHOL PETROLATUM LANOLIN CETYL ESTERS WAX COCOA BUTTER PARAFFIN YELLOW WAX SALICYLIC ACID PHENOL"
                ],
                "warnings": [
                    "Warnings For external use only. When using this product, keep out of eyes. Rinse with water to remove. Stop use and consult a doctor if: condition worsens or rash occurs. Keep out of reach of children. If swallowed, get medical help or contact a Poison Control Center right away."
                ],
                "spl_unclassified_section": [
                    "CARMEX original lip balm EXTERNAL ANALGESIC"
                ]
            }
        ]
    };

    callback(null, JSON.stringify(data));
};

data.findProductUPC = function(thisUPC, callback) {
    if (thisUPC === '999999999999') {
        //Simulate upc  not found
        callback(null, null);
        return;
    }

    if (thisUPC === 'somethingreallybad') {
        //Simulate server error
        var data = {
            "error": {
                "code": "SERVER_ERROR"
            }
        };

        callback(null, JSON.stringify(data));
        return;
    }

    var data =  {
        "meta": {
            "disclaimer": "openFDA is a beta research project and not for clinical use. While we make every effort to ensure that data is accurate, you should assume all results are unvalidated.",
            "license": "http://open.fda.gov/license",
            "last_updated": "2015-05-31",
            "results": {
                "skip": 0,
                "limit": 25,
                "total": 12
            }
        },
        "results": [
            {
                "set_id": "0f5b2c4c-55f2-4140-b706-f260e2c96cbc",
                "indications_and_usage": [
                    "Uses: For temporary relief of the symptoms of dry lips"
                ],
                "keep_out_of_reach_of_children": [
                    "Keep out of reach of children. If swallowed, get medical help or contact a Poison Control Center right away."
                ],
                "dosage_and_administration": [
                    "Directions apply to lips not more than 3 to 4 times daily children under 2 years of age: ask a doctor"
                ],
                "purpose": [
                    "Purpose External Analgesic"
                ],
                "version": "3",
                "id": "b7f6fb80-3ead-46a1-97fe-0d63a224de6b",
                "package_label_principal_display_panel": [
                    "CARMEX original lip balm EXTERNAL ANALGESIC 7.5g (10210-0011-0)| CARMEX original lip balm EXTERNAL ANALGESIC 7.5g/Jar in Blister Pack (10210-0011-1) CARMEXExternalAnalgesic2 CARMEXExternalAnalgesic3 JarBlisterCard"
                ],
                "active_ingredient": [
                    "Active Ingredients Camphor 1.7% Menthol 0.7% Purpose External Analgesic"
                ],
                "inactive_ingredient": [
                    "Inactive Ingredients petrolatum, lanolin, cetyl esters, theobroma cacao (cocoa) seed butter, paraffin, beeswax, flavor, salicylic acid, phenol"
                ],
                "@epoch": 1415927453.475662,
                "effective_time": "20140814",
                "openfda": {
                    "spl_id": [
                        "b7f6fb80-3ead-46a1-97fe-0d63a224de6b"
                    ],
                    "product_ndc": [
                        "10210-0011"
                    ],
                    "rxcui": [
                        "1439969",
                        "1439974"
                    ],
                    "substance_name": [
                        "MENTHOL",
                        "CAMPHOR (NATURAL)"
                    ],
                    "spl_set_id": [
                        "0f5b2c4c-55f2-4140-b706-f260e2c96cbc"
                    ],
                    "product_type": [
                        "HUMAN OTC DRUG"
                    ],
                    "upc": [
                        "0083078011307"
                    ],
                    "manufacturer_name": [
                        "Carma Laboratories, Inc."
                    ],
                    "brand_name": [
                        "CARMEX original lip balm EXTERNAL ANALGESIC"
                    ],
                    "is_original_packager": [
                        true
                    ],
                    "route": [
                        "TOPICAL"
                    ],
                    "package_ndc": [
                        "10210-0011-0",
                        "10210-0011-1"
                    ],
                    "generic_name": [
                        "CAMPHOR (NATURAL), MENTHOL"
                    ],
                    "application_number": [
                        "part348"
                    ]
                },
                "spl_product_data_elements": [
                    "CARMEX original lip balm EXTERNAL ANALGESIC CAMPHOR (NATURAL), MENTHOL CAMPHOR (NATURAL) CAMPHOR (NATURAL) MENTHOL MENTHOL PETROLATUM LANOLIN CETYL ESTERS WAX COCOA BUTTER PARAFFIN YELLOW WAX SALICYLIC ACID PHENOL"
                ],
                "warnings": [
                    "Warnings For external use only. When using this product, keep out of eyes. Rinse with water to remove. Stop use and consult a doctor if: condition worsens or rash occurs. Keep out of reach of children. If swallowed, get medical help or contact a Poison Control Center right away."
                ],
                "spl_unclassified_section": [
                    "CARMEX original lip balm EXTERNAL ANALGESIC"
                ]
            }
        ]
    };

    callback(null, JSON.stringify(data));
};

data.getIngredientInfo = function(device, callback) {
    var products = [
        {
            "upc": "0312090070017",
            "manufacturer_name": "Summit Industries, Inc.",
            "brand_name": "Lantiseptic Daily Care Skin Protectant",
            "package_label_principal_display_panel": "Images of representative container artwork 4oz.jpg Label Multipack",
            "image_urls": [
                "http://ecx.images-amazon.com/images/I/51o9aqugKtL._SL500_AA300_.jpg",
                "http://pics2.ds-static.com/prodimg/310794/300.JPG",
                "http://www.walgreens.comhttp://pics.drugstore.com/prodimg/310794/220.jpg"
            ],
            "ingredients": [
                "Lanolin Usp Skin Protectant Beeswax (Apis Mellifera)Yellow Wax",
                "Disodium Edta",
                "Fragrance"
            ]
        }];

    callback(null, products);
};

data.findFactualUPC = function(thisProduct, callback) {
    var deferred = Q.defer();
    setTimeout(function () {
        var response = {data: [{
                avg_price: 11.89,
                brand: "Carmex",
                category: "Skin Care",
                ean13: "0083078011307",
                factual_id: "4087dee1-228e-48d2-a9a3-3665efb00361",
                image_urls: [
                    "http://ecx.images-amazon.com/images/I/21t87%2Bm9daL._SL500_AA300_.jpg",
                    "http://image01.bizrate-images.com/resize?uid=856855637",
                    "http://static-resources.goodguide.net/images/entities/large/207299.jpg",
                    "http://w00.static-wize.com/photos/large/1027915.jpg",
                    "http://www.myotcstore.com/store/i/is.aspx?path=/images/C-productimages/Carmex/130914.jpg"
                ],
                ingredients: [
                    "Octinoxate",
                    "Oxybenzone",
                    "Petrolatum",
                    "Lanolin"
                ],
                product_name: "Cold Sore Reliever And Lip Moisturizer Cherry Jar",
                size: [
                    "0.25 oz"
                ],
                upc: "083078011307"
            }]
        };

        deferred.resolve(response);
    }, 0);

    return deferred.promise;
};

data.getIrritants = function(callback) {
    var deferred = Q.defer();

    setTimeout(function () {
        var data = [
            {
                _id: "558b5ec5f0683004e3a626a0",
                category: "Metals",
                irritant: "Aluminum"
            },
            {
                _id: "558b5ec6f0683004e3a626a1",
                category: "Metals",
                irritant: "Nickel"
            },
            {
                _id: "558b5ec6f0683004e3a626a2",
                category: "Metals",
                irritant: "Chromium"
            },
            {
                _id: "558b5ec6f0683004e3a626a3",
                category: "Metals",
                irritant: "Lead"
            },
            {
                _id: "558b5ec6f0683004e3a626a4",
                category: "Acids",
                irritant: "Salicylic Acid"
            }
        ];

        deferred.resolve(data);
    }, 0);

    return deferred.promise;
};

data.getProfile = function(email_addr, callback) {
    if (email_addr === 'jimmmm.peters@hp.com') {
        //Simulate email  not found
        callback(null, null);
        return;
    }

    var data = {
        "_id": "558cbe114cc88525bd555fca",
        "name": "Jim Peters",
        "email_addr": "jim.peters@hp.com",
        "ingredients": [
            "Aluminum",
            "Nickel",
            "Chrome",
            "lanolin"
        ],
        "products": [
            {
                "UPC": "0083078113148",
                "brand_name": "Carmex",
                "image_url": "http://w00.static-wize.com/photos/large/1027915.jpg"
            }
        ]
    };

    callback(null, data);
};

data.insertProfile = function(email_addr, profileObj, callback) {
    if (email_addr === "" || email_addr == null){
        callback(new Error('Test Error'));
        return;
    }

    callback(null);
}

data.findFactualID = function(thisProduct, callback) {
    if (thisProduct === '222-222-2222-2222') {
        //Simulate not found
        callback(null, null);
        return;
    }

    var data = {
        "data": [
            {
                "avg_price": 11.89,
                "brand": "Carmex Mock Data",
                "category": "Skin Care",
                "ean13": "0083078011307",
                "factual_id": "4087dee1-228e-48d2-a9a3-3665efb00361",
                "image_urls": [
                    "http://ecx.images-amazon.com/images/I/21t87%2Bm9daL._SL500_AA300_.jpg",
                    "http://image01.bizrate-images.com/resize?uid=856855637",
                    "http://static-resources.goodguide.net/images/entities/large/207299.jpg",
                    "http://w00.static-wize.com/photos/large/1027915.jpg",
                    "http://www.myotcstore.com/store/i/is.aspx?path=/images/C-productimages/Carmex/130914.jpg"
                ],
                "ingredients": [
                    "Octinoxate",
                    "Oxybenzone",
                    "Petrolatum",
                    "Lanolin",
                    "Cocoa Butter",
                    "Mineral Oil",
                    "Camphor",
                    "Menthol",
                    "Phenol In A Mixture Of Waxes",
                    "Flavor"
                ],
                "product_name": "Cold Sore Reliever And Lip Moisturizer Cherry Jar",
                "size": [
                    "0.25 oz"
                ],
                "upc": "083078011307"
            }
        ],
        "included_rows": 1,
        "total_row_count": 1
    };

    callback(null, data);
};
