/**
 * Created by jimpeters on 6/24/15.
 */
var should = require('should');
var assert = require('assert');
var request = require('supertest');

var port = 8000;

describe('testgetIngredientInfo', function () {
    it ('should return 400 for missing parameter', function (done) {
        var input = {};
        input.ingreients = [];
        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/getIngredientInfo')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('post parameter missing');

                done();
            });
    });

    it ('should return 404 for not found', function (done) {
        var input = {};
        input.ingredients = ["somethingbad"];
        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/getIngredientInfo')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(404)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Not Found');

                done();
            });
    });

    it ('should return 500 for SERVER_ERROR', function (done) {
        var input = {};
        input.ingredients = ["somethingreallybad"];
        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/getIngredientInfo')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(500)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.body.result.should.equal('error');
                res.body.reason.should.equal('Server error');

                done();
            });
    });

    it ('should return 200 for a valid call', function (done) {
        var input = {};
        input.ingredients = ["lanolin", "Lead", "Chromium", "Cyclopentasiloxane"];
        this.timeout(5000);
        request('http://localhost:' + port)
            .post('/api/getIngredientInfo')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                done();
            });
    });
});
