/**
 * Created by jimpeters on 6/24/15.
 */
var express = require('express');
var router = express.Router();
var Q = require('q');

router.get('/', function(req, res) {
    var dataLayer = req.app.locals.dataLayer;
    var utils = req.app.locals.utils;

    Q.denodeify(dataLayer.getIrritants().then(function(data) {
        if (data === null || data.length === 0) {
            utils.handleError(new Error('Not Found'), res);
        } else {
            var irritantsMisc = [];
            var irritantsFragrance = [];
            var irritantsEmollient = [];
            var irritantsSulfate = [];
            var irritantsEssentialOil = [];

            for (var i = 0; i < data.length; i++) {
                if ("irritantsMisc" === data[i].category) {
                    irritantsMisc.push(data[i].irritant);
                } else if ("irritantsFragrance" === data[i].category) {
                    irritantsFragrance.push(data[i].irritant);
                } else if ("irritantsEmollient" === data[i].category) {
                    irritantsEmollient.push(data[i].irritant);
                } else if ("irritantsSulfate" === data[i].category) {
                    irritantsSulfate.push(data[i].irritant);
                } else {
                    irritantsEssentialOil.push(data[i].irritant);
                }
            }

            var irritants = {};
            irritants.irritantsMisc = irritantsMisc;
            irritants.irritantsFragrance = irritantsFragrance;
            irritants.irritantsEmollient = irritantsEmollient;
            irritants.irritantsSulfate = irritantsSulfate;
            irritants.irritantsEssentialOil = irritantsEssentialOil;

            var thisResult = {};
            thisResult.irritants = irritants;

            res.status(200).json(thisResult);
        }
    }, function(err){
        utils.handleError(new Error('Not Found'), res);
    }));
});

module.exports = router;

