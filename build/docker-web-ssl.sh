#!/bin/bash

# Remove running instances
running=$(sudo docker ps -a -q -f 'name=gsa-prod-web')

for i in $running;
do
        echo "Stopping Web Container"
        sudo docker stop $i
        echo "Removing Web Container"
        sudo docker rm $i
done

#Create the new image
sudo docker build -t gsa-nodejs-ssl web/.

#Start a container with this image
sudo docker run --name gsa-prod-web --link gsa-prod-db:db -v /opt/ssl:/ssl -e "NODE_ENV=production" -p 443:3000 -d gsa-nodejs-ssl