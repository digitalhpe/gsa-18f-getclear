module.exports = function (grunt) {
    var path;
    path = {
        libs: [
            'bower_components/modernizr/modernizr.js',
            'bower_components/jquery-2.1.0.min/index.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/underscore/underscore.js',
            'bower_components/backbone/backbone.js',
            'bower_components/marionette/lib/backbone.marionette.min.js',
            'bower_components/handlebars/handlebars.js',
            'common/lib/template_helpers.js'
        ],
        css: [
            'bower_components/bootstrap/dist/css/bootstrap.css',
            'common/css/**/*.css'
        ]
    };

    grunt.initConfig({

        env: {
            options: {
                //Shared Options Hash
            },
            test: {
                NODE_ENV: 'test'
            }
        },

        express: {
            options: {
                // Override defaults here
                background: true
            },
            test: {
                options: {
                    script: 'bin/www',
                    port: '8000'
                }
            }
        },

        shell: {
            startService: {
                command: 'node bin/www &',
            }
        },

        sonarRunner: {

            analysis: {
                options: {
                    debug: true,
                    separator: '\n',
                    sonar: {
                        host: {
                            url: 'https://tool-mobile.external.hp.com/sonar/'
                        },
                        jdbc: {
                            url: 'jdbc:mysql://10.0.0.10:3306/sonar?autoReconnect=true&useUnicode=true&characterEncoding=utf8',
                            username: 'sonar',
                            password: '5yNeCLVbB8'
                        },

                        projectKey: 'gsa-18f',
                        projectName: 'GSA-18F',
                        projectVersion: '0.1',
                        sources: ['routes', 'public/clientjs/js', 'public/clientjs/view'].join(','),
                        language: 'js',
                        sourceEncoding: 'UTF-8',
                        login: 'jenkins',
                        password: 'nrHmPux3DKGzN2G'
                    }
                }
            }
        },

        jshint: {
            files: ['routes/*.js', 'app.js', 'public/clientjs/js/*.js', 'public/clientjs/route/*.js', 'public/clientjs/view/*.js', 'public/clientjs/test/*.js', 'public/clientjs/app.js', 'public/clientjs/base.js'],
            options: {
                reporter: 'jslint',
                reporterOutput: 'output/lint_results.xml',
                force: true
            }
        },

        mochaTest: {
            test: {
                options: {
                    reporter: 'xunit',
                    captureFile: 'output/unit_test_results.xml', // Optionally capture the reporter output to a file
                    quiet: false, // Optionally suppress output to standard out (defaults to false)
                    clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
                },
                src: ['test/**/*.js', 'public/clientjs/test/*.js']
            }
        },

        jasmine: {
            run: {
                src: [
                    'public/clientjs/view/*.js',
                    'public/clientjs/js/web_service_calls.js',
                    'public/clientjs/js/utils.js'
                ],
                options: {
                    specs: 'public/clientjs/test/specs/*.js',
                    vendor: [
                        'public/clientjs/bower_components/underscore/underscore.js',
                        'public/clientjs/bower_components/jquery-2.1.0.min/index.js',
                        'public/clientjs/bower_components/backbone/backbone.js',
                        'public/clientjs/bower_components/marionette/lib/backbone.marionette.min.js',
                        'public/clientjs/data/gc_data.js',
                        'public/clientsjs/view/*.js',
                        'public/clientjs/app.js',
                        'public/clientsjs/base.js'

                    ],
                    junit: {
                        path: 'output'
                    }
                }
            }
        },

        handlebars: {
            compile: {
                options: {
                    namespace: "GC.Template",
                    processName: function (filePath) {
                        var fileName = filePath.match(/.*\/([a-zA-Z0-9\_\-]+)\.html$/);
                        return fileName[1];
                    }
                },
                files: {
                    "public/clientjs/templates.js": ["public/clientjs/template/*.html"]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-bower');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-shell-spawn');
    grunt.loadNpmTasks('grunt-sonar-runner');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-env');

    grunt.registerTask('lint', ['jshint', 'sonarRunner:analysis']);
    grunt.registerTask('test', ['env:test', 'express:test', 'mochaTest', 'jasmine']);
    grunt.registerTask('dev', ['lint', 'test']);
    grunt.registerTask('startService', ['handlebars', 'shell:startService']);
    grunt.registerTask('build', ['handlebars']);
}
;