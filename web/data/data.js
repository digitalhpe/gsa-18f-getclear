/**
 * Created by jimpeters on 6/23/15.
 */
var Factual = require('factual-api');
var request = require("request");
var config = require('config');

var Q = require('q');
var factual = new Factual(config.get("factualKey"), config.get("factualSecret"));
//factual.startDebug();
var mongo = require('mongodb');
var monk = require('monk');
var db = monk(config.get('connectionString'));
var irritantsTable = db.get('irritant');
var profileTable = db.get('profile');

var data = module.exports = {};

data.findFactualID = function(thisProduct, callback) {
    var endpoint = '/t/products-cpg-nutrition';
    var queryString = thisProduct;
    var includeCount = 'true';

    factual.get(endpoint, {q:queryString,"include_count":includeCount}, function (error, response) {
        if (error) {
            callback(error, null);
        } else if (response === null) {
            callback(new Error('Not Found'), null);
        } else {
            callback(null, response);
        }
    });
};

data.findFactualUPC = function(thisProduct, callback) { 
    console.log("Factual UPC = " + thisProduct);
    var deferred = Q.defer(); 
    var endpoint = '/t/products-cpg-nutrition'; 
    var queryString = thisProduct; 
    var includeCount = 'true';  
    factual.get(endpoint, {q:queryString,"include_count":includeCount}, function (error, response) { 
        if (error || response == null) { 
            deferred.reject(error); 
        } else { 
            deferred.resolve(response); 
        } 
    });  

    return deferred.promise;
 };

data.findFactualName = function(thisProduct, callback) {
    var endpoint = '/t/products-cpg-nutrition';
    var queryString = thisProduct;
    var includeCount = 'true';

    factual.get(endpoint, {q:queryString,"include_count":includeCount}, function (error, response) {
        if (error) {
            callback(error, null);
        } else if (response === null) {
            callback(new Error('Not Found'), null);
        } else {
            callback(null, response);
        }
    });
};

data.findProduct = function(thisProduct, callback) {
    var endpoint = 'https://api.fda.gov/drug/label.json?api_key=4RGgb5QdkZkqU2xsBSvN4t0HOTGNaUmqru7auRDW&search=(_exists_:(upc)+AND+brand_name:(\"';
    var queryString = thisProduct;
    var tail = '\"))&limit=15';

    var opts = endpoint + queryString + tail;

    request(opts, function (error, response, body) {
        if (error) {
            callback(error, null);
        } else if (response === null) {
            callback(new Error('Not Found'), null);
        } else {
            callback(null, body);
        }
    });
};

data.findActiveIngredient = function(ingredientString, callback) {
    var endpoint = 'https://api.fda.gov/drug/label.json?api_key=4RGgb5QdkZkqU2xsBSvN4t0HOTGNaUmqru7auRDW&search=(_exists_:(upc)+AND+active_ingredient:(';
    var queryString = ingredientString;
    var tail = '))&limit=15';

    var opts = endpoint + queryString + tail;

    request(opts, function (error, response, body) {
        if (error) {
            callback(error, null);
        } else if (response === null) {
            callback(new Error('Not Found'), null);
        } else {
            callback(null, body);
        }
    });
};

data.findProductUPC = function(thisUPC, callback) {
    var endpoint = 'https://api.fda.gov/drug/label.json?api_key=4RGgb5QdkZkqU2xsBSvN4t0HOTGNaUmqru7auRDW&search=upc:(';
    var queryString = thisUPC;
    var tail = ')+route:(topical)';

    var opts = endpoint + queryString + tail;

    request(opts, function (error, response, body) {
        if (error) {
            callback(error, null);
        } else if (response === null) {
            callback(new Error('Not Found'), null);
        } else {
            callback(null, body);
        }
    });
};

data.getIrritants = function(callback) {
    var deferred = Q.defer();
    irritantsTable.find({}, {}, function (error, data) {
        if (error) {
            deferred.reject(error);
        } else {
            deferred.resolve(data);
        }
    });

    return deferred.promise;
};

data.getProfile = function(email_addr, callback) {
    profileTable.find({"email_addr" : email_addr }, function (error, data) {
        if (error) {
            callback(error, null);
        } else {
            callback(error, data);
        }
    });
};

data.insertProfile = function(email_addr, profileObj, callback) {
    data.deleteProfile(email_addr, function(error) {
        if (error) {
            //utils.handleError(err, res);
        } else {
            profileTable.insert(profileObj, function (error) {
                callback(error);
            });
        }
    });
};

data.deleteProfile = function(email_addr, callback) {
    profileTable.remove({"email_addr" : email_addr }, function (error) {
        callback(error);
    });
};

