Handlebars.registerHelper("modulusOf", function(index_count,mod,block) {
    if(parseInt(index_count)%(mod)=== 0) {
        return block.fn(this);
    }
});

Handlebars.registerHelper("nextModulusOf", function(index_count,mod,block) {
    if(parseInt(index_count + 1)%(mod)=== 0) {
        return block.fn(this);
    }
});

Handlebars.registerHelper("foreach",function(arr,options) {
    if(options.inverse && !arr.length)
        return options.inverse(this);

    return arr.map(function(item,index) {
        item.$index = index;
        item.$first = index === 0;
        item.$last  = index === arr.length-1;
        return options.fn(item);
    }).join('');
});