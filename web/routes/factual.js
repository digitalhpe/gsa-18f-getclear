var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var data = req.app.locals.dataLayer;
    var utils = req.app.locals.utils;

    if (req.query.FactualID === undefined) {
        res.status(400).json({"result": 'get parameter missing'});
        return;
    }

    var thisProduct = req.query.FactualID;

    if (!thisProduct) {
        res.status(400).json({"result": 'get parameter empty'});
        return;
    }

    data.findFactualID(thisProduct, function(err, data) {
        if (err) {
            utils.handleError(err, res);
        } else if (data === null || data.length === 0) {
            utils.handleError(new Error('Not Found'), res);
        } else {
            res.status(200).json(data);
        }
    });
});

module.exports = router;
