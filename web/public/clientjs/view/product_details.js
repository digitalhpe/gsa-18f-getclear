GC.View.product_details = Backbone.Marionette.ItemView.extend({

    template: GC.Template.product_details,

    events: {
        "click #known-irritants-button": "saveIrritants"
    },

    initialize: function (upc) {
        var _self = this;
        _self.upc = upc.param;
    },

    determineIrritants: function () {

        var _self = this;
        var foundIrritants = [];

        // Ingredients from Factual
        var ingredients = _self.product.ingredients.toString().toUpperCase();
        // Ingredients from the FDA
        var inactive_ingredients = _self.product.inactive_ingredient.toUpperCase();
        var active_ingredients = _self.product.active_ingredient.toUpperCase();

        // Concatenate our known irritants object into a list
        var knownIrritants = [];
        var irritantsObject = GC.Data.app.irritants;
        for (var irritantCategory in irritantsObject) {
            if (irritantsObject.hasOwnProperty(irritantCategory)) {
                knownIrritants = knownIrritants.concat(irritantsObject[irritantCategory]);
            }
        }

        // Compare our known irritants against all of the product's ingredients
        for (var irritant in knownIrritants) {
            if (knownIrritants.hasOwnProperty(irritant)) {
                var knownIrritant = knownIrritants[irritant];

                var toCompare = knownIrritant.toString().toUpperCase();
                if ((ingredients.indexOf(toCompare) > 0) || (inactive_ingredients.indexOf(toCompare) > 0) || (active_ingredients.indexOf(toCompare) > 0)) {
                    foundIrritants.push(knownIrritant);
                }
            }
        }
        _self.product.knownIrritants = foundIrritants;
    },

    onRender: function () {

        var _self = this;
        if (!GC.Data.app.irritants) {
            GC.WebServiceCalls.getIrritants(function (data) {
                GC.Data.app.irritants = data.irritants;
                _self.render();
            }, function (data) {
                console.error(data.responseText);
            });
        } else {
            GC.WebServiceCalls.getProductUpc(_self.upc, function (data) {

                _self.product = data.products[0];
                _self.determineIrritants();
                _self.$el.html(_self.template(_self.product));

                if (_self.product.knownIrritants.length > 0) {
                    if (GC.Data.app.user.authenticated === true) {
                        $('#known-irritants-button').show();
                    }
                    $('#known-irritants-title').show();
                }

                if (GC.Data.app.user.authenticated === true) {
                    $('#known-irritants-button').show();

                    $(".knownIrritant").click(function () {
                        var irritantName = this.id;
                        $('#submit-success').hide();
                        $('#submit-failure').hide();
                        var index = GC.Data.app.selectedIrritants.indexOf(irritantName);

                        if (index > -1) {
                            GC.Data.app.selectedIrritants.splice(index, 1);
                            $(this).addClass("btn-default");
                            $(this).removeClass("btn-primary");
                        } else {
                            GC.Data.app.selectedIrritants.push(irritantName);
                            $(this).addClass("btn-primary");
                            $(this).removeClass("btn-default");
                        }

                        $('#known-irritants-submit').prop("disabled", GC.Data.app.selectedIrritants.length < 1);
                    });
                } else {
                    $(".knownIrritant").prop("disabled", true);
                }

                return this;
            }, function (data) {
                console.error(data.responseText);
                $('#data-load-failure').show();
            });
        }
    },

    saveIrritants: function () {
        for (var irritant in  GC.Data.app.selectedIrritants) {
            if (GC.Data.app.selectedIrritants.hasOwnProperty(irritant)) {
                var index = GC.Data.app.user.knownIrritantsList.indexOf(GC.Data.app.selectedIrritants[irritant]);
                if (index < 0) {
                    GC.Data.app.user.knownIrritantsList.push(GC.Data.app.selectedIrritants[irritant]);
                }
            }
        }

        $(".knownIrritant").addClass("btn-default");
        $(".knownIrritant").removeClass("btn-primary");

        GC.WebServiceCalls.updateProfile(function () {
            $('#submit-success').show();
        }, function () {
            $('#submit-failure').show();
        });
    }
});